const BN = require("bn.js");
const IdeaTokenFormula = artifacts.require("IdeaTokenBondingCurve");

const decimal = new BN("1" + "0".repeat(18));

const supplyIntervalThreshold = new BN(100).mul(decimal);
const formulaThreshold = new BN(10000).mul(decimal);
const basePrice = new BN(1);
const priceRise = new BN("1" + "0".repeat(16)); //10 ** 16
const sellRatio = 2;

// Valid buying prices.
const buy_supply0nb1000 = new BN(1000).mul(decimal).mul(basePrice); // total supply: 0,  nb wei: 1000 * (10^18)
const buy_supply10nb9990 = new BN(9990).mul(decimal).mul(basePrice); // total supply: 10 * (10^18), nb wei: 9990 * (10^18)
const buy_supply9999nb15 = new BN(1)
  .mul(decimal)
  .mul(basePrice)
  .add(new BN(14).mul(decimal).mul(basePrice))
  .add(new BN(14).mul(priceRise)); // total supply: 9999 * (10 ^ 18), nb wei: 15 * (10^18)
const buy_supply10000nb1 = new BN(1)
  .mul(decimal)
  .mul(basePrice)
  .add(new BN(1).mul(priceRise)); // total supply: 10000 * (10^18), nb wei: 1 * (10^18)
const buy_supply10000nb105 = new BN(105)
  .mul(decimal)
  .mul(basePrice)
  .add(new BN(100).mul(priceRise))
  .add(new BN(5).mul(new BN(2).mul(priceRise))); // total supply: 10000 * (10^18), nb wei: 105 * (10^18)
const buy_supply0nb19695 = new BN(2444315).mul(new BN("1" + "0".repeat(16))); // total supply: 0, nb wei: 19695 * (10^18)
const buy_supply0nb1000000007wei = new BN("1000000007").mul(basePrice);
const buy_supply10000nb1000000100wei = new BN("1000000100")
  .mul(basePrice)
  .add(new BN("1000000100").div(new BN(100)));
const buy_supply10000nb1000000000wei = new BN("1000000000")
  .mul(basePrice)
  .add(new BN("1000000000").div(new BN(100)));

// Utility function for getting selling price based on buying price and sellRation.
const applyRatio = (buyPrice, ratio) => {
  return buyPrice.mul(new BN(100 - ratio)).div(new BN(100));
};

contract("IdeaTokenFormula", async () => {
  let ideaTokenFormula;
  before(async () => {
    ideaTokenFormula = await IdeaTokenFormula.new(
      supplyIntervalThreshold,
      priceRise,
      basePrice,
      formulaThreshold,
      sellRatio
    );
  });

  it("has correct buying price formula", async () => {
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(new BN(0), new BN(1))).eq(
        basePrice
      ),
      "bad buying price for supply: 0  nb wei: 1"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(0),
        new BN(1000).mul(decimal)
      )).eq(buy_supply0nb1000),
      "bad buying price for supply: 0  nb wei: 1000* (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(10).mul(decimal),
        new BN(9990).mul(decimal)
      )).eq(buy_supply10nb9990),
      "bad buying price for supply: 10 * (10^18)  nb wei: 9990 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(9999).mul(decimal),
        new BN(15).mul(decimal)
      )).eq(buy_supply9999nb15),
      "bad buying price for supply: 9999 * (10^18)  nb wei: 15 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(10000).mul(decimal),
        new BN(1).mul(decimal)
      )).eq(buy_supply10000nb1),
      "bad buying price for supply: 10000 * (10^18)  nb wei: 1 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(10000).mul(decimal),
        new BN(105).mul(decimal)
      )).eq(buy_supply10000nb105),
      "bad buying price for supply: 10000 * (10^18)  nb wei: 105 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(0).mul(decimal),
        new BN(19695).mul(decimal)
      )).eq(buy_supply0nb19695),
      "bad buying price for supply: 0  nb wei: 19695 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(0),
        new BN("1000000007")
      )).eq(buy_supply0nb1000000007wei),
      "bad buying price for supply: 0  nb wei: 1000000007"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(10000).mul(decimal),
        new BN("1000000100")
      )).eq(buy_supply10000nb1000000100wei),
      "bad buying price for supply: 10000 * (10^18)  nb wei: 1000000100"
    );
    assert.isOk(
      (await ideaTokenFormula.getBuyPrice.call(
        new BN(10000).mul(decimal),
        new BN("1000000007")
      )).eq(buy_supply10000nb1000000100wei),
      "bad buying price for supply: 10000 * (10^18)  nb wei: 1000000007"
    );
  });

  it("has correct selling price formula", async () => {
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(1000).mul(decimal),
        new BN(1000).mul(decimal)
      )).eq(applyRatio(buy_supply0nb1000, sellRatio)),
      "bad selling price for supply: 1000 * (10^18)  nb wei: 1000 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(10000).mul(decimal),
        new BN(9990).mul(decimal)
      )).eq(applyRatio(buy_supply10nb9990, sellRatio)),
      "bad selling price for supply: 10000 * (10^18)  nb wei: 9990 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(10014).mul(decimal),
        new BN(15).mul(decimal)
      )).eq(applyRatio(buy_supply9999nb15, sellRatio)),
      "bad selling price for supply: 10014 * (10^18)  nb wei: 15 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(10001).mul(decimal),
        new BN(1).mul(decimal)
      )).eq(applyRatio(buy_supply10000nb1, sellRatio)),
      "bad selling price for supply: 10001 * (10^18)  nb wei: 1 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(10105).mul(decimal),
        new BN(105).mul(decimal)
      )).eq(applyRatio(buy_supply10000nb105, sellRatio)),
      "bad selling price for supply: 10105 * (10^18)  nb wei: 105 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN(19695).mul(decimal),
        new BN(19695).mul(decimal)
      )).eq(applyRatio(buy_supply0nb19695, sellRatio)),
      "bad selling price for supply: 19695 * (10^18)  nb wei: 19695 * (10^18)"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        new BN("1000000007"),
        new BN("1000000007")
      )).eq(applyRatio(buy_supply0nb1000000007wei, sellRatio)),
      "bad selling price for supply: 1000000007 nb wei: 1000000007"
    );
    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        formulaThreshold.add(new BN("1000000007")),
        new BN("1000000007")
      )).eq(applyRatio(buy_supply10000nb1000000000wei, sellRatio)),
      "bad selling price for supply: 10000 * (10^18) + 1000000007 nb wei: 1000000007"
    );

    assert.isOk(
      (await ideaTokenFormula.getSellPrice.call(
        formulaThreshold.add(new BN("1000000100")),
        new BN("1000000100")
      )).eq(applyRatio(buy_supply10000nb1000000100wei, sellRatio)),
      "bad selling price for supply: 10000 * (10^18) + 1000000100 nb wei: 1000000100"
    );
  });
});
