const MockDai = artifacts.require("openzeppelin/MockDai");
const MockAllocationStrategy = artifacts.require(
  "openzeppelin/MockAllocationStrategy"
);
const RToken = artifacts.require("rtoken-contracts/RToken");
const IdeaTokenFactory = artifacts.require("IdeaTokenFactory");
const IdeaToken = artifacts.require("IdeaToken");

contract("Contract", async accounts => {
  let mockDai;
  let mockAllocationStrategy;
  let rToken;
  let ideaTokenFactory;
  const minCashOutCoolDown = 240;
  const additionalNonOwnerCashOutCoolDown = 10;
  const payoutCapThresholdNum = 1;
  const payoutCapThresholdDenom = 100;
  const globalPayoutDust = 10 ** 15;

  const decimals = 18;
  const decimalScale = 10 ** decimals;
  const decimalStr = "0".repeat(18);

  const R = (100 * decimalScale).toString();
  const Ci = decimalScale / 100;
  const B = 1;
  const Q = (10000 * decimalScale).toString();
  const totalDai = "100000" + decimalStr;

  const domainStr = "github.com";

  async function mintDai(addr = accounts[0]) {
    await mockDai.mint(addr, totalDai);
  }

  async function buyXIdeaTokens(quantity) {
    return buyXIdeaTokensForDomain(quantity, domainStr);
  }

  async function buyXIdeaTokensForDomain(quantity, domain) {
    await mockDai.approve(rToken.address, totalDai);
    await rToken.mint(totalDai);
    const cost = (await ideaTokenFactory.getCostOfIdeaToken.call(
      domain,
      quantity
    )).toString();
    await rToken.approve(ideaTokenFactory.address, cost);

    return await ideaTokenFactory.buyIdeaToken(domain, quantity);
  }

  async function sellXIdeaTokens(quantity) {
    return await ideaTokenFactory.sellIdeaToken(domainStr, quantity);
  }

  async function createIdeaTokenAndMint() {
    return createIdeaTokenAndMintForDomain(domainStr);
  }

  async function createIdeaTokenAndMintForDomain(domain) {
    await mintDai(accounts[0]);
    await mockDai.approve(rToken.address, "10" + decimalStr);
    await rToken.mint("10" + decimalStr);
    await rToken.approve(ideaTokenFactory.address, "10" + decimalStr);
    await ideaTokenFactory.createIdeaToken(domain);
    await mintDai(accounts[0]);
  }

  async function init(
    _oldFactoryAddress,
    _minCashOutCoolDown,
    _additionalNonOwnerCashOutCoolDown, 
    _payoutCapThresholdNum, 
    _payoutCapThresholdDenom,
    _globalPayoutDust) {
    mockDai = await MockDai.new();
    console.log("MockDai:", mockDai.address);
    mockAllocationStrategy = await MockAllocationStrategy.new(mockDai.address);
    rToken = await RToken.new(mockAllocationStrategy.address);
    console.log("RToken:", rToken.address);

    ideaTokenFactory = await IdeaTokenFactory.new(
      _oldFactoryAddress,
      rToken.address,
      accounts[1],
      _minCashOutCoolDown,
      _additionalNonOwnerCashOutCoolDown,
      _payoutCapThresholdNum,
      _payoutCapThresholdDenom,
      _globalPayoutDust
    );
    console.log("IdeaTokenFactory:", ideaTokenFactory.address);
  }

  beforeEach(async () => {
    return init(
      "0x0000000000000000000000000000000000000000",
      minCashOutCoolDown,
      additionalNonOwnerCashOutCoolDown,
      payoutCapThresholdNum,
      payoutCapThresholdDenom,
      globalPayoutDust
      );
  });

  contract("MockDai", async () => {
    it("can mint tokens for a given account", async () => {
      assert.equal(
        await mockDai.totalSupply.call(),
        0,
        "Initial Total Supply is not zero"
      );
      await mintDai(accounts[0]);
      assert.equal(
        await mockDai.balanceOf.call(accounts[0]),
        totalDai,
        "mint() is not adding tokens to the given address"
      );
      assert.equal(
        await mockDai.totalSupply.call(),
        totalDai,
        "mint() did not mint correct number of tokens"
      );
    });
  });

  contract("MockAllocationStrategy", async () => {
    beforeEach(async () => {
      // await mintDai(accounts[0]);
    });

    it("has underlying asset as DAI", async () => {
      assert.equal(
        await mockAllocationStrategy.underlying.call(),
        mockDai.address
      );
    });

    it("can invest", async () => {
      await mintDai(accounts[0]);
      assert.isOk(await mockAllocationStrategy.investUnderlying(totalDai));
    });
  });

  contract("IdeaTokenFactory", async () => {

    it("can payout interest", async () => {
      
      await init(
        "0x0000000000000000000000000000000000000000",
        0,
        0,
        payoutCapThresholdNum,
        payoutCapThresholdDenom,
        0);

      await rToken.setFixedInterestPayout(100000000000000);

      await createIdeaTokenAndMintForDomain("test.com");
      await createIdeaTokenAndMintForDomain("example.com");

      // Get some odd market cap distribution
      await buyXIdeaTokensForDomain("3" + decimalStr, "test.com");
      await buyXIdeaTokensForDomain("1" + decimalStr, "example.com");


      const testToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("test.com"));
      const exampleToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("example.com"));
      
      const testTokenStartBalance = await rToken.balanceOf.call(testToken.address);
      const exampleTokenStartBalance = await rToken.balanceOf.call(exampleToken.address);

      await ideaTokenFactory.distributeInterest();

      const testTokenEndBalance = await rToken.balanceOf.call(testToken.address);
      const exampleTokenEndBalance = await rToken.balanceOf.call(exampleToken.address);

      assert.isTrue(testTokenEndBalance.sub(testTokenStartBalance).eq(new web3.utils.BN("175000000000000")), "wrong test token amount");
      assert.isTrue(exampleTokenEndBalance.sub(exampleTokenStartBalance).eq(new web3.utils.BN("125000000000000")), "wrong example token amount");
    });

    it("interest on token with small market cap", async () => {
        
      await init(
        "0x0000000000000000000000000000000000000000",
        0,
        0,
        payoutCapThresholdNum,
        payoutCapThresholdDenom,
        0);

      await rToken.setFixedInterestPayout(100000000000000);

      await createIdeaTokenAndMintForDomain("test.com");
      await createIdeaTokenAndMintForDomain("example.com");

      // example token has low market cap
      await buyXIdeaTokensForDomain("150" + decimalStr, "test.com");
      await buyXIdeaTokensForDomain("1" + decimalStr, "example.com");

      const testToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("test.com"));
      const exampleToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("example.com"));
      
      const testTokenStartBalance = await rToken.balanceOf.call(testToken.address);
      const exampleTokenStartBalance = await rToken.balanceOf.call(exampleToken.address);

      await ideaTokenFactory.distributeInterest();

      const testTokenEndBalance = await rToken.balanceOf.call(testToken.address);
      const exampleTokenEndBalance = await rToken.balanceOf.call(exampleToken.address);

      const pendingInterest = await ideaTokenFactory.pendingInterest.call();

      assert.isTrue(testTokenEndBalance.sub(testTokenStartBalance).eq(new web3.utils.BN("200000000000000")), "wrong test token amount");
      assert.isTrue(exampleTokenEndBalance.eq(exampleTokenStartBalance), "wrong example token amount");
      assert.isTrue(pendingInterest.eq(new web3.utils.BN("100000000000000")), "wrong pending interest");
    });

    it("can create new IdeaToken", async () => {
      await mintDai(accounts[0]);
      assert.equal(await ideaTokenFactory.getTokenCount.call(), 0);
      await mockDai.approve(rToken.address, "10" + decimalStr);
      await rToken.mint("10" + decimalStr);
      await rToken.approve(ideaTokenFactory.address, "10" + decimalStr);
      await ideaTokenFactory.createIdeaToken(domainStr);
      assert.equal(await ideaTokenFactory.getTokenCount.call(), 1);
      assert.notEqual(
        await ideaTokenFactory.getPublicationContractAddress.call(domainStr),
        "0x0000000000000000000000000000000000000000"
      );
      assert.equal(
        await ideaTokenFactory.getTokenAccountBalance.call(
          domainStr,
          accounts[0]
        ),
        0
      );
    });

    it("initial cost of new token to be B", async () => {
      await createIdeaTokenAndMint();
      assert.equal(
        (await ideaTokenFactory.getCostOfIdeaToken.call(
          domainStr,
          1
        )).toString(),
        B
      );
    });

    it("domain string check", async () => {
      assert.isTrue(
        await ideaTokenFactory.domainStringCheck.call("domain_1-23.com")
      );
      assert.isFalse(
        await ideaTokenFactory.domainStringCheck.call("domain_1-23^")
      );
      assert.isFalse(
        await ideaTokenFactory.domainStringCheck.call("domain123.com/dir")
      );
    });

    it("can buy idea tokens", async () => {
      await createIdeaTokenAndMint();
      assert.isOk(await buyXIdeaTokens("10000" + decimalStr));
    });

    it("can sell idea token", async () => {

      await createIdeaTokenAndMint();

      assert.equal(
        (await (await IdeaToken.at(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).getBuyPrice.call(0, "10000" + decimalStr)).toString(),
        "5050" + decimalStr,
        "buy price before"
      );

      await buyXIdeaTokens("10000" + decimalStr);
      assert.equal(
        await ideaTokenFactory.getTokenAccountBalance.call(
          domainStr,
          accounts[0]
        ),
        "10000" + decimalStr,
        "itf balance"
      );

      assert.equal(
        (await rToken.balanceOf.call(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).toString(),
        "5050" + decimalStr,
        "rtoken balance in ideatoken"
      );

      assert.equal(
        await (await IdeaToken.at(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).applyInterestMultiplier.call(100),
        100,
        "interestmultiplier = 1"
      );

      assert.equal(
        (await (await IdeaToken.at(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).getBuyPrice.call(0, "10000" + decimalStr)).toString(),
        "5050" + decimalStr,
        "buy price after"
      );

      assert.equal(
        await (await IdeaToken.at(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).totalSupply.call(),
        "10000" + decimalStr,
        "total supply"
      );

      assert.equal(
        (await ideaTokenFactory.getPriceOfIdeaTokens.call(
          domainStr,
          "10000" + decimalStr
        )).toString(),
        "4949" + decimalStr, // 98%
        "sell price"
      );

      assert.isOk(await sellXIdeaTokens("10000" + decimalStr));

      assert.equal(
        (await ideaTokenFactory.getTokenAccountBalance.call(
          domainStr,
          accounts[0]
        )).toString(),
        0,
        "account balance: 0 idea tokens"
      );

      assert.equal(
        (await rToken.balanceOf.call(
          await ideaTokenFactory.getPublicationContractAddress.call(domainStr)
        )).toString(),
        "101" + decimalStr, // 2%
        "ideatoken rdai balance"
      );
      assert.equal(
        (await rToken.balanceOf.call(accounts[0])).toString(),
        "99909" + decimalStr, // 98%
        "rdai balance"
      );
    });

    it("can transfer state", async () => {
      await createIdeaTokenAndMintForDomain("example.com");
      await createIdeaTokenAndMintForDomain("test.com");

      const exampleToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("example.com"));
      const testToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call("test.com"));

      const oldFactory = await IdeaTokenFactory.at(ideaTokenFactory.address);
      const newFactory = await IdeaTokenFactory.new(
        oldFactory.address,
        rToken.address,
        accounts[1],
        minCashOutCoolDown,
        additionalNonOwnerCashOutCoolDown,
        payoutCapThresholdNum,
        payoutCapThresholdDenom,
        globalPayoutDust
      );

      await buyXIdeaTokensForDomain("10" + decimalStr, "example.com");
      await buyXIdeaTokensForDomain("20" + decimalStr, "test.com");

      assert.equal((await exampleToken.balanceOf.call(accounts[0])).toString(), "10" + decimalStr, "example token wrong balance");
      assert.equal((await testToken.balanceOf.call(accounts[0])).toString(), "20" + decimalStr, "test token wrong balance");

      assert.isFalse(await oldFactory.stopped.call(), "old factory should not be stopped");
      assert.isTrue(await oldFactory.initialized.call(), "old factory should be initialized");
      assert.isFalse(await newFactory.initialized.call(), "new factory should not be initialized");

      assert.equal((await exampleToken.baseContract.call()).toString(), oldFactory.address, "old factory should be base contract");
      assert.equal((await testToken.baseContract.call()).toString(), oldFactory.address, "old factory should be base contract");

      await rToken.setFixedInterestPayout(100000000000000);
      await oldFactory.transferState(newFactory.address);

      assert.equal((await exampleToken.balanceOf.call(accounts[0])).toString(), "10" + decimalStr, "example token wrong balance");
      assert.equal((await testToken.balanceOf.call(accounts[0])).toString(), "20" + decimalStr, "test token wrong balance");

      assert.isTrue(await oldFactory.stopped.call(), "old factory should be stopped");
      assert.isTrue(await newFactory.initialized.call(), "new factory should be initialized");

      assert.equal((await exampleToken.baseContract.call()).toString(), newFactory.address, "new factory should be base contract");
      assert.equal((await testToken.baseContract.call()).toString(), newFactory.address, "new factory should be base contract");
    });
  });
});
