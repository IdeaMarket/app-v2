const MockDai = artifacts.require("openzeppelin/MockDai");
const MockAllocationStrategy = artifacts.require(
  "openzeppelin/MockAllocationStrategy"
);
const RToken = artifacts.require("rtoken-contracts/RToken");
const IdeaTokenFactory = artifacts.require("IdeaTokenFactory");
const IdeaToken = artifacts.require("IdeaToken");
const MockUniswapFactory = artifacts.require("MockUniswapFactory");
const MockUniswapExchange = artifacts.require("MockUniswapExchange");
const UserProxyRegistry = artifacts.require("UserProxyRegistry");
const UserProxy = artifacts.require("UserProxy");
const ERC20 = artifacts.require("ERC20");

contract("Contract", async accounts => {
  let mockDai;
  let mockAllocationStrategy;
  let mockUniswapFactory;
  let rToken;
  let ideaTokenFactory;
  let userProxyRegistry;
  let userProxy;

  const minCashOutCoolDown = 240;
  const additionalNonOwnerCashOutCoolDown = 10;
  const payoutCapThresholdNum = 1;
  const payoutCapThresholdDenom = 100;
  const globalPayoutDust = 10 ** 15;

  const decimals = 18;
  const decimalScale = 10 ** decimals;
  const decimalStr = "0".repeat(18);
  const uint256Max = new web3.utils.BN(2).pow(new web3.utils.BN(256)).sub(new web3.utils.BN(1));

  const R = (100 * decimalScale).toString();
  const Ci = decimalScale / 100;
  const B = 1;
  const Q = (10000 * decimalScale).toString();
  const totalDai = "100000" + decimalStr;

  const domainStr = "github.com";

  async function mintDai(addr = accounts[0]) {
    await mockDai.mint(addr, totalDai);
  }

  async function buyXIdeaTokens(quantity) {
    return buyXIdeaTokensForDomain(quantity, domainStr);
  }

  async function buyXIdeaTokensForDomain(quantity, domain) {
    await mockDai.approve(rToken.address, totalDai);
    await rToken.mint(totalDai);
    const cost = (await ideaTokenFactory.getCostOfIdeaToken.call(
      domain,
      quantity
    )).toString();
    await rToken.approve(ideaTokenFactory.address, cost);

    return await ideaTokenFactory.buyIdeaToken(domain, quantity);
  }

  async function createIdeaTokenAndMint() {
    return createIdeaTokenAndMintForDomain(domainStr);
  }

  async function createIdeaTokenAndMintForDomain(domain) {
    await mintDai(accounts[0]);
    await mockDai.approve(rToken.address, "10" + decimalStr);
    await rToken.mint("10" + decimalStr);
    await rToken.approve(ideaTokenFactory.address, "10" + decimalStr);
    await ideaTokenFactory.createIdeaToken(domain);
    await mintDai(accounts[0]);
  }

  async function init(
    _oldFactoryAddress,
    _minCashOutCoolDown,
    _additionalNonOwnerCashOutCoolDown, 
    _payoutCapThresholdNum, 
    _payoutCapThresholdDenom,
    _globalPayoutDust) {
    mockDai = await MockDai.new();
    mockAllocationStrategy = await MockAllocationStrategy.new(mockDai.address);
    rToken = await RToken.new(mockAllocationStrategy.address);

    ideaTokenFactory = await IdeaTokenFactory.new(
      _oldFactoryAddress,
      rToken.address,
      accounts[1],
      _minCashOutCoolDown,
      _additionalNonOwnerCashOutCoolDown,
      _payoutCapThresholdNum,
      _payoutCapThresholdDenom,
      _globalPayoutDust
    );
    console.log("IdeaTokenFactory:", ideaTokenFactory.address);

    mockUniswapFactory = await MockUniswapFactory.new();
    console.log("MockUniswapFactory:", mockUniswapFactory.address)

    userProxyRegistry = await UserProxyRegistry.new(
      ideaTokenFactory.address,
      mockDai.address,
      rToken.address,
      mockUniswapFactory.address
    );
    console.log("UserProxyRegistry:", userProxyRegistry.address);

    await userProxyRegistry.deployProxy()
    userProxy = await UserProxy.at(await userProxyRegistry.getUserProxyAddress.call(accounts[0]));
    console.log("UserProxy:", userProxy.address);
  }

  beforeEach(async () => {
    return init(
      "0x0000000000000000000000000000000000000000",
      minCashOutCoolDown,
      additionalNonOwnerCashOutCoolDown,
      payoutCapThresholdNum,
      payoutCapThresholdDenom,
      globalPayoutDust
      );
  });

  contract("UserProxy", async () => {
    it("can buy idea token with dai", async () => {

      await createIdeaTokenAndMint();
      const ideaToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call(domainStr));
      const ideaTokenBalanceBefore = await ideaToken.balanceOf.call(accounts[0]); 

      const daiBalanceBefore = await mockDai.balanceOf.call(accounts[0]);
      const daiInputAmount = new web3.utils.BN("210" + decimalStr);
      const ideaTokenBuyAmount = new web3.utils.BN("2000" + decimalStr); 

      await mockDai.approve(userProxy.address, uint256Max);
      await userProxy.buyIdeaTokens(
        mockDai.address, 
        daiInputAmount,
        ideaTokenBuyAmount,
        ideaTokenBuyAmount,
        domainStr  
      )

      const ideaTokenBalanceAfter = await ideaToken.balanceOf.call(accounts[0]); 
      const daiBalanceAfter = await mockDai.balanceOf.call(accounts[0]);

      assert.isTrue(ideaTokenBalanceAfter.sub(ideaTokenBalanceBefore).eq(new web3.utils.BN(ideaTokenBuyAmount)), "invalid idea token amount");
      assert.isTrue(daiBalanceBefore.sub(daiBalanceAfter).eq(daiInputAmount), "invalid dai amount");
    });
    
    it("can buy idea token with rDai", async () => {

      await createIdeaTokenAndMint();
      const ideaToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call(domainStr));
      const ideaTokenBalanceBefore = await ideaToken.balanceOf.call(accounts[0]); 

      const rdaiInputAmount = new web3.utils.BN("210" + decimalStr);
      const ideaTokenBuyAmount = new web3.utils.BN("2000" + decimalStr); 

      const daiBalanceBefore = await mockDai.balanceOf.call(accounts[0]);
      const rdaiBalanceBefore = await rToken.balanceOf.call(accounts[0]);
      await mockDai.approve(rToken.address, rdaiInputAmount);
      await rToken.mint(rdaiInputAmount);
      
      await rToken.approve(userProxy.address, uint256Max);
      await userProxy.buyIdeaTokens(
        rToken.address, 
        rdaiInputAmount,
        ideaTokenBuyAmount,
        ideaTokenBuyAmount,
        domainStr  
      )

      const daiBalanceAfter = await mockDai.balanceOf.call(accounts[0]);
      const rdaiBalanceAfter = await rToken.balanceOf.call(accounts[0]);
      const ideaTokenBalanceAfter = await ideaToken.balanceOf.call(accounts[0]); 

      assert.isTrue(ideaTokenBalanceAfter.sub(ideaTokenBalanceBefore).eq(new web3.utils.BN(ideaTokenBuyAmount)), "invalid idea token amount");
      assert.isTrue(rdaiBalanceBefore.eq(rdaiBalanceAfter), "invalid rdai amount");
      assert.isTrue(daiBalanceBefore.sub(daiBalanceAfter).eq(rdaiInputAmount), "invalid dai amount");
    });

    it("can buy idea token with eth", async () => {

      const daiPoolAmount = new web3.utils.BN("3000" + decimalStr);
      const ethPoolAmount = new web3.utils.BN("10" + decimalStr);
      await mockDai.mint(accounts[0], daiPoolAmount);

      await mockUniswapFactory.createExchange(mockDai.address);
      const daiExchange = await MockUniswapExchange.at(await mockUniswapFactory.getExchange.call(mockDai.address));
      await mockDai.approve(daiExchange.address, daiPoolAmount);
      await daiExchange.addLiquidity(0, daiPoolAmount, Date.now(), {value: ethPoolAmount});

      const daiBalanceAfterPool = await mockDai.balanceOf.call(accounts[0]);
      assert.isTrue(daiBalanceAfterPool.eq(new web3.utils.BN(0)), "dai balance should be zero after adding to pool");

      await createIdeaTokenAndMint();
      const ideaToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call(domainStr));
      const ideaTokenBalanceBefore = await ideaToken.balanceOf.call(accounts[0]); 

      // By supplying 1 eth we get ~271.98 DAI with which we can buy 2000 Idea tokens
      const ethInputAmount = new web3.utils.BN("1" + decimalStr);
      const expectedIdeaTokensAmount = new web3.utils.BN("2000" + decimalStr);
      await userProxy.buyIdeaTokens("0x0000000000000000000000000000000000000000", 0, expectedIdeaTokensAmount, expectedIdeaTokensAmount, domainStr, {value: ethInputAmount});

      const ideaTokenBalanceAfter = await ideaToken.balanceOf.call(accounts[0]); 

      assert.isTrue(ideaTokenBalanceAfter.sub(ideaTokenBalanceBefore).eq(expectedIdeaTokensAmount), "wrong idea tokens amount");
    });

    it("can buy idea token with token", async () => {
   
      const daiPoolAmount = new web3.utils.BN("3000" + decimalStr);
      const tokenPoolAmount = new web3.utils.BN("3000" + decimalStr);
      const ethPoolAmount = new web3.utils.BN("10" + decimalStr);

      // We use MockDai as token because it has mint built in
      const token = await MockDai.new();
      await token.mint(accounts[0], tokenPoolAmount);
      await mockDai.mint(accounts[0], daiPoolAmount);

      await mockUniswapFactory.createExchange(mockDai.address);
      const daiExchange = await MockUniswapExchange.at(await mockUniswapFactory.getExchange.call(mockDai.address));
      await mockDai.approve(daiExchange.address, daiPoolAmount);
      await daiExchange.addLiquidity(0, daiPoolAmount, Date.now(), {value: ethPoolAmount});

      await mockUniswapFactory.createExchange(token.address);
      const tokenExchange = await MockUniswapExchange.at(await mockUniswapFactory.getExchange.call(token.address));
      await token.approve(tokenExchange.address, tokenPoolAmount);
      await tokenExchange.addLiquidity(0, tokenPoolAmount, Date.now(), {value: ethPoolAmount});

      const daiBalanceAfterPool = await mockDai.balanceOf.call(accounts[0]);
      const tokenBalanceAfterPool = await token.balanceOf.call(accounts[0]);
      assert.isTrue(daiBalanceAfterPool.eq(new web3.utils.BN(0)), "dai balance should be zero after adding to pool");
      assert.isTrue(tokenBalanceAfterPool.eq(new web3.utils.BN(0)), "token balance should be zero after adding to pool");

      await token.mint(accounts[0], new web3.utils.BN("300" + decimalStr));
      await createIdeaTokenAndMint();
      const ideaToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call(domainStr));
      const ideaTokenBalanceBefore = await ideaToken.balanceOf.call(accounts[0]); 

      // By supplying 300 token we get ~248.68 DAI with which we can buy 2000 Idea tokens
      const tokenInputAmount = new web3.utils.BN("300" + decimalStr);
      const expectedIdeaTokensAmount = new web3.utils.BN("2000" + decimalStr);
      await userProxy.buyIdeaTokens(token.address, tokenInputAmount, expectedIdeaTokensAmount, expectedIdeaTokensAmount, domainStr);

      const ideaTokenBalanceAfter = await ideaToken.balanceOf.call(accounts[0]); 
      assert.isTrue(ideaTokenBalanceAfter.sub(ideaTokenBalanceBefore).eq(expectedIdeaTokensAmount), "wrong idea tokens amount");
    });

    it("fail buy slippage too high", async () => {

      await createIdeaTokenAndMint();
      const ideaToken = await IdeaToken.at(await ideaTokenFactory.getPublicationContractAddress.call(domainStr));
      const ideaTokenBalanceBefore = await ideaToken.balanceOf.call(accounts[0]); 

      const daiBalanceBefore = await mockDai.balanceOf.call(accounts[0]);
      const daiInputAmount = new web3.utils.BN("10" + decimalStr);
      const ideaTokenBuyAmount = new web3.utils.BN("10" + decimalStr); 

      await mockDai.approve(userProxy.address, uint256Max);
      try {
        await userProxy.buyIdeaTokens(
          mockDai.address, 
          daiInputAmount,
          new web3.utils.BN("10000" + decimalStr),
          new web3.utils.BN("10000" + decimalStr),
          domainStr  
        )
        assert.isTrue(false, "should have reverted because of slippage too high");
      } catch(err) {
        return;
      }
    });

    it("fail buy invalid token", async () => {
      try {
        await userProxy.buyIdeaTokens("0x0000000000000100000000002000000000300000", 1, 1, 1, domainStr);
        assert.isTrue(false, "should have reverted because of invalid token");
      } catch(err) {
        return;
      }
    });
  });
});
