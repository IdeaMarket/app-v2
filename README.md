# IdeaMarket

[![codecov](https://codecov.io/gl/idea-markets/idea-markets/branch/master/graph/badge.svg?token=RBytEWSzE9)](https://codecov.io/gl/idea-markets/idea-markets) [![Coverage Status](https://coveralls.io/repos/gitlab/idea-markets/idea-markets/badge.svg?t=9gg4sT)](https://coveralls.io/gitlab/idea-markets/idea-markets)

A protocol for establishing credibility without trusting third parties. Short the media, long idea markets.

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
npx quasar dev
```

### Lint the files

```bash
yarn run lint
```

### Build the app for production

```bash
npx quasar build
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
