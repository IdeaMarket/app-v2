pragma solidity ^0.5.8;

import "./openzeppelin/SafeMath.sol";
import "./IdeaToken.sol";
import "./openzeppelin/ReentrancyGuard.sol";
import "./rtoken-contracts/IRToken.sol";

contract IdeaTokenFactory is ReentrancyGuard {
    using SafeMath for uint256;

    mapping(uint256 => address) internal domainIdToAddress;

    address[] public tokenContractAddresses;

    IRToken rDaiContract;

    uint256 public marketCap;
    uint256 public pendingInterest;


    uint256 minCashOutCoolDown;
    uint256 additionalNonOwnerCashOutCoolDown;
    uint256 payoutCapThresholdNum;
    uint256 payoutCapThresholdDenom;
    uint256 globalPayoutDust;

    uint256 lastDistribution;
    uint256 hatId;

    address devFund;

    address owner;

    address oldFactory;
    bool public initialized;
    bool public stopped;

    modifier onlyOwner {
        require(owner == msg.sender, "This method is for owner only");
        _;
    }

    modifier notStopped {
        require(!stopped, "Contract has been stopped");
        _;
    }

    modifier isInitialized {
        require(initialized, "Contract is not initialized");
        _;
    }

    modifier notInitialized {
        require(!initialized, "Contract already initialized");
        _;
    }

    /* EVENTS */
    event IdeaTokenCreated(
        string indexed domain,
        address newContractAddress,
        uint blockNumber
    );
    event IdeaTokenBought(
        string indexed domain,
        address publicationAddress,
        address indexed buyer,
        uint cost,
        uint marketCapBefore,
        uint marketCapAfter,
        uint totalMarketCap,
        uint timestamp
    );
    event IdeaTokenSold(
        string indexed domain,
        address publicationAddress,
        address indexed seller,
        uint price,
        uint marketCapBefore,
        uint marketCapAfter,
        uint totalMarketCap,
        uint timestamp
    );
    event IdeaTokenInterestPaid(
        address publicationAddress,
        uint amount
    );
    event StateTransferred(
        address newFactory
    );
    event StateReceived(
        address oldFactory
    );
    /* END EVENTS */

    constructor(
        address _oldFactory,
        address payable _rDaiContractAddress,
        address _devFund,
        uint256 _minCashOutCoolDown,
        uint256 _additionalNonOwnerCashOutCoolDown,
        uint256 _payoutCapThresholdNum,
        uint256 _payoutCapThresholdDenom,
        uint256 _globalPayoutDust) public {

        owner = msg.sender;
        oldFactory = _oldFactory;
        rDaiContract = IRToken(_rDaiContractAddress);
        minCashOutCoolDown = _minCashOutCoolDown;
        additionalNonOwnerCashOutCoolDown = _additionalNonOwnerCashOutCoolDown;
        payoutCapThresholdNum = _payoutCapThresholdNum;
        payoutCapThresholdDenom = _payoutCapThresholdDenom;
        globalPayoutDust = _globalPayoutDust;
        devFund = _devFund;
        stopped = false;

        if(oldFactory == address(0)) {
            initialized = true;
        } else {
            initialized = false;
        }

        address[] memory hataddrs = new address[](1);
        uint32[] memory hatdistr = new uint32[](1);

        hataddrs[0] = address(this);
        hatdistr[0] = uint32(100);

        rDaiContract.createHat(hataddrs, hatdistr, true);
    }

    function setOwner(address _newOwner) external onlyOwner {
        owner = _newOwner;
    }

    function transferState(address _newFactory) external onlyOwner notStopped isInitialized {

        /*
        * We need to distribute interest here because rDAi
        * automatically pays accumulated interest when being transfered.
        * This would be missed by the factory.
        */
        distributeInterestInternal(false);

        for(uint i = 0; i < tokenContractAddresses.length; i++) {
            IdeaToken token = IdeaToken(tokenContractAddresses[i]);
            token.changeBaseContract(_newFactory);
        }

        rDaiContract.transferAll(_newFactory);
        stopped = true;
        IdeaTokenFactory(_newFactory).receiveState(tokenContractAddresses, marketCap, pendingInterest, lastDistribution);

        emit StateTransferred(_newFactory);
    }

    function receiveState(
        address[] calldata _tokenContractAddresses,
        uint _marketCap,
        uint _pendingInterest,
        uint _lastDistribution) external notStopped notInitialized {

        require(msg.sender == oldFactory, "Can only be called by old factory");

        marketCap = _marketCap;
        pendingInterest = _pendingInterest;
        lastDistribution = _lastDistribution;

        for(uint i = 0; i < _tokenContractAddresses.length; i++) {
            IdeaToken token = IdeaToken(_tokenContractAddresses[i]);
            tokenContractAddresses.push(address(token));
            domainIdToAddress[token.domainId()] = _tokenContractAddresses[i];
        }

        initialized = true;
        emit StateReceived(oldFactory);
    }

    function domainStringCheck(string memory str) public pure returns (bool) {
        //restricts domain names to 0-9 and a-z and - (hyphen) only
        bytes memory b = bytes(str);
        uint256 dotcount = 0;

        for(uint i = 0; i < b.length; i++) {
            bytes1 char = b[i];
            if (
                !(char >= 0x30 && char <= 0x39) && //9-0
                !(char >= 0x61 && char <= 0x7A) && //a-z
                !(char == 0x2D) &&//-
                !(char == 0x2E) &&//.
                !(char == 0x5F) //_
            )
                return false;

             if (char == 0x2E) { // .
                 dotcount++;
             }
        }

         if (dotcount != 1) {
             return false;
         }

        return true;
    }

    function getPublicationContractAddress(string memory _domainStr) public view returns (address) {
        return domainIdToAddress[domainStrToId(_domainStr)];
    }

    function domainStrToId(string memory _domainStr) public pure returns (uint256) {
        return uint(keccak256(abi.encodePacked(_domainStr)));
    }

    function _createTokenContract(string memory _domainStr) internal returns (address) {
        uint256 _basePrice = 1;
        uint  _domainId = domainStrToId(_domainStr);
        address payable rDaiContractAddress = address(uint160(address(rDaiContract)));

        IdeaToken newToken = new IdeaToken(
            address(this),
            rDaiContractAddress,
            devFund,
            _domainId,
            _domainStr,
            100 * (10 ** 18),
            (10 ** 16),
            _basePrice.div(100),
            0 * (10 ** 18));

        address newContractAddress = address(newToken);
        domainIdToAddress[_domainId] = newContractAddress;
        tokenContractAddresses.push(newContractAddress);

        return newContractAddress;
    }

    function createIdeaToken(string calldata _domainStr ) external nonReentrant notStopped isInitialized {
        require(domainStringCheck(_domainStr), "domain string check failed");
        require(getPublicationContractAddress(_domainStr) == address(0), "domain already deployed");
        address newContractAddress = _createTokenContract(_domainStr);

        emit IdeaTokenCreated(_domainStr, newContractAddress, block.number);
    }

    function getPublicationTokenSupply(string calldata _domainStr) external view returns (uint) {
        return IdeaToken(getPublicationContractAddress(_domainStr)).totalSupply();
    }

    function getTokenAccountBalance(string memory _domainStr, address _checkAddress) public view returns (uint) {
        return IdeaToken(getPublicationContractAddress(_domainStr)).balanceOf(_checkAddress);
    }

    function getCostOfIdeaToken(string memory _domainStr,uint _qty) public view returns (uint) {
        IdeaToken newitfContract = IdeaToken(getPublicationContractAddress(_domainStr));
        return newitfContract.getCostOfBuyingTokens(_qty);
    }

    function getPriceOfIdeaTokens(string memory _domainStr,uint _qty) public view returns (uint) {
        IdeaToken newitfContract = IdeaToken(getPublicationContractAddress(_domainStr));
        return newitfContract.getPriceofSellingTokens(_qty);
    }

    function buyIdeaToken(string calldata _domainStr,uint _qty) external nonReentrant notStopped isInitialized {
        require(_qty >= 1, "buyIdeaToken: Must buy at least 1!");

        address publicationAddress = getPublicationContractAddress(_domainStr);
        uint cost = getCostOfIdeaToken(_domainStr, _qty);
        uint allowance = rDaiContract.allowance(msg.sender, address(this));
        require(allowance >= cost, "buyIdeaToken: not enough dai allowance");

        rDaiContract.transferFrom(msg.sender, publicationAddress, cost);
        IdeaToken newitfContract = IdeaToken(publicationAddress);
        uint mcapBefore = newitfContract.marketCap();
        newitfContract.buyTokens(_qty, msg.sender, cost);
        uint mcapAfter = newitfContract.marketCap();

        marketCap = marketCap + (mcapAfter - mcapBefore);

        emit IdeaTokenBought(
            _domainStr,
            publicationAddress,
            msg.sender,
            cost,
            mcapBefore,
            mcapAfter,
            marketCap,
            now
        );
    }

    function sellIdeaToken(string calldata _domainStr,uint _qty) external nonReentrant notStopped isInitialized {
        require(_qty >= 1, "Must sell at least 1");
        require(getTokenAccountBalance(_domainStr, msg.sender) >= _qty, "Not enough tokens");

        address publicationAddress = getPublicationContractAddress(_domainStr);
        uint price = getPriceOfIdeaTokens(_domainStr,_qty);
        require(rDaiContract.balanceOf(publicationAddress) >= price, "not enough rdai to payout");

        IdeaToken newitfContract = IdeaToken(publicationAddress);
        uint mcapBefore = newitfContract.marketCap();
        newitfContract.sellTokens(_qty, msg.sender, price);
        uint mcapAfter = newitfContract.marketCap();

        marketCap = marketCap + (mcapAfter - mcapBefore);

        newitfContract.rDaiTransfer(msg.sender, price);

        emit IdeaTokenSold(
            _domainStr,
            publicationAddress,
            msg.sender,
            price,
            mcapBefore,
            mcapAfter,
            marketCap,
            now
        );
    }

    function getTokenCount() public view returns(uint) {
        return tokenContractAddresses.length;
    }

    ///////////////////////////
    // Interest Distribution //
    ///////////////////////////

    function rDaiBalance() public view returns (uint256) {
        return rDaiContract.balanceOf(address(this));
    }

    function setPayoutCapThreshold(uint256 _payoutCapThresholdNum, uint256 _payoutCapThresholdDenom)
        external
        onlyOwner
        notStopped
        isInitialized
    {
        payoutCapThresholdNum = _payoutCapThresholdNum;
        payoutCapThresholdDenom = _payoutCapThresholdDenom;
    }

    function setCoolDown(uint256 _minCashOutCoolDown, uint256 _additionalNonOwnerCashOutCoolDown)
        external
        onlyOwner
        notStopped
        isInitialized
    {
        minCashOutCoolDown = _minCashOutCoolDown;
        additionalNonOwnerCashOutCoolDown = _additionalNonOwnerCashOutCoolDown;
    }

    function setGlobalPayoutDust(uint256 _globalPayoutDust)
        external
        onlyOwner
        notStopped
        isInitialized
    {
        globalPayoutDust = _globalPayoutDust;
    }

    function sendRDai(address to, uint amount) internal {
        rDaiContract.transfer(to, amount);
    }

    function distributeInterest() external nonReentrant notStopped isInitialized {
        require(
            block.number.sub(minCashOutCoolDown) > lastDistribution,
            "Must wait for cooldown to cash out"
        );

        // if owner goes offline, give everyone else an opportunity to trigger payout
        require(
            owner == msg.sender || block.number.sub(minCashOutCoolDown + additionalNonOwnerCashOutCoolDown) > lastDistribution,
            "Must wait for cooldown to cash out"
        );

        distributeInterestInternal(true);
    }

    /**
     * Returns which tokens are eligible to receive interest
     * and their total market cap.
    **/
    function getTokensEligibleForInterest() internal view returns(uint, IdeaToken[] memory, uint) {
        uint256 totalCap = marketCap;
        uint256 tokensCap = totalCap;
        uint tokenCount = getTokenCount();
        uint numIdeaTokensToPay;
        IdeaToken[] memory ideaTokensToPay = new IdeaToken[](tokenCount);

        // Find out which idea tokens have enough market cap to receive interest
        for (uint i = 0; i < tokenCount; i++) {
            address tokenaddr = tokenContractAddresses[i];
            IdeaToken token = IdeaToken(tokenaddr);
            uint256 cap = token.marketCap();
            if (cap == 0 || cap.mul(payoutCapThresholdDenom) < totalCap.mul(payoutCapThresholdNum)) {
                tokensCap = tokensCap.sub(cap);
                continue;
            }

            ideaTokensToPay[numIdeaTokensToPay] = token;
            numIdeaTokensToPay = numIdeaTokensToPay.add(1);
        }

        return(numIdeaTokensToPay, ideaTokensToPay, tokensCap);
    }

    /**
     * Normally we only want the owner to be able to distribute interest, but
     * if the owner goes offline we want other people to be able to trigger an
     * interest distribution too
     */
    function distributeInterestInternal(bool checkDust) internal {

        uint beforeBalance = rDaiContract.balanceOf(address(this));
        rDaiContract.payInterest(address(this));
        uint afterBalance = rDaiContract.balanceOf(address(this));

        // Include pending interest
        uint interestEarned = afterBalance - beforeBalance;
        uint balance = interestEarned + pendingInterest;

        if(checkDust) {
            require(balance > globalPayoutDust, "payout too small");
        }
        

        (uint numIdeaTokensToPay, IdeaToken[] memory ideaTokensToPay, uint tokensCap) = getTokensEligibleForInterest();

        // There are no tokens to be paid out. Store the earned interest for the next time.
        if(numIdeaTokensToPay == 0) {
            pendingInterest = pendingInterest.add(interestEarned);
            return;
        }

        uint256 remaining = rDaiBalance();
        uint totalSent = 0;
        // Pending interest will be paid out now.
        pendingInterest = 0;

        for(uint i = 0; i < numIdeaTokensToPay; i++) {

            IdeaToken token = ideaTokensToPay[i];
            address tokenaddr = address(token);
            uint256 cap = token.marketCap();

            // Division is flooring, this might turn out to be 1 too few
            uint entitledAmount = balance.mul(cap).div(tokensCap);
            remaining = remaining.sub(entitledAmount);
            totalSent = totalSent.add(entitledAmount);

            uint beforeSendBalance = rDaiBalance();
            sendRDai(tokenaddr, entitledAmount);
            uint afterSendBalance = rDaiBalance();

            uint accumulatedInterest;
            if(afterSendBalance > beforeSendBalance) {
                uint diff = afterSendBalance.sub(beforeSendBalance);
                accumulatedInterest = entitledAmount.add(diff);
            } else {
                uint diff = beforeSendBalance.sub(afterSendBalance);
                accumulatedInterest = entitledAmount.sub(diff);
            }

            // If interest was accumulated while transfering store it as pending interest.
            pendingInterest = pendingInterest.add(accumulatedInterest);
            remaining = remaining.add(accumulatedInterest);

            emit IdeaTokenInterestPaid(tokenaddr, entitledAmount);
        }

        uint roundingError = balance.sub(totalSent);
        // Can be off by 1 for each token paid out
        require(roundingError <= numIdeaTokensToPay, "rounding error too big");

        // Rounding error will be paid out next time
        pendingInterest = pendingInterest.add(roundingError);

        require(remaining == rDaiBalance(), "wrong number of dai distributed");

        lastDistribution = block.number;
    }

    /** @dev Get a list of publications.
     *
     * @return An array of the publications' addresses.
     */
    function getPublications() public view returns (address[] memory) { return tokenContractAddresses; }
}
