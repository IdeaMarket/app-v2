pragma solidity ^0.5.8;
import "./openzeppelin/SafeMath.sol";

/// @title The idea token price bonding curve
/// @author ["Sofiane Akermoun <akersof@gmail.com>"]
/// @notice You can use this contract by inheriting the IdeaToken contract from IdeaTokenBondingCurve
/// @dev All function calls are currently implemented without side effects
/// @dev All price calculations and value are in wei
contract IdeaTokenBondingCurve {
    using SafeMath for uint256;

    uint256 constant private DECIMAL = 10 ** 18;
    // Price rising calculation is triggered from the totalTokensWithoutPriceRise supplied tokens
    uint256 private totalTokensWithoutPriceRise;

    // The base price of a 1 wei. This is the price of the first totalTokensWithoutPriceRise supplied tokens
    uint256 private basePrice;

    // From FormulaThreshold supplied tokens, for each tokensInEachInterval tokens a rising of priceRisePerInterval is applied to basePrice
    uint256 private tokensInEachInterval;

    // The price increase of priceRisePerInterval per tokensInEachInterval after FormulaThreshold supplied tokens.
    uint256 private  priceRisePerInterval;

    // Sell fee in percents. The sell price is the buy price minus the lockedTokenPercentage.
    uint256 private lockedTokenPercentage;

    // This is the minimum of IdeaToken a user can sell/buy. The reason is below 100 wei we can't apply a percentage
    // if the user purchase 1000000000000000001 IdeaWei then it will be round up to 1000000000000000100 and a price calculation is perform
    // if the user sell 1000000000000000001 IdeaWei then it will be round down to 1000000000000000000 and a price calculation is perform
    uint256 constant private MIN_WEI_UNIT = 100;

    constructor(uint256 _tokensInEachInterval, uint256 _priceRisePerInterval, uint256 _basePrice, uint256 _totalTokensWithoutPriceRise, uint256 _lockedTokenPercentage) public {
        tokensInEachInterval = _tokensInEachInterval;
        priceRisePerInterval = _priceRisePerInterval;
        basePrice = _basePrice;
        totalTokensWithoutPriceRise = _totalTokensWithoutPriceRise;
        lockedTokenPercentage = _lockedTokenPercentage;
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @param _totalSupply number of supplied tokens. For a purchase, _totalSupply = 'already supplied tokens' + 'nb tokens to purchase'
    /// @return number of completed tokensInEachInterval from 0 to _totalSupply
    function getCompleteSteps(uint256 _totalSupply) private view returns(uint256){
        if(_totalSupply <= totalTokensWithoutPriceRise) return 0;
        return _totalSupply.sub(totalTokensWithoutPriceRise).div(tokensInEachInterval);
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @param _totalSupply number of supplied tokens. For a purchase, _totalSupply = 'already supplied tokens' + 'nb tokens to purchase'
    /// @return number of remaining tokens in an incomplete step.
    function getRemainingTokensInIncompleteStep(uint256 _totalSupply) private view returns(uint256) {
        if(_totalSupply <= totalTokensWithoutPriceRise) return 0;
        return _totalSupply.mod(tokensInEachInterval);
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @param _totalSupply number of supplied tokens.
    /// return the number of tokens rounded up to the nearest whole wei hundred
    function roundNbTokensForBuying(uint256 _totalSupply) private view returns(uint256) {
        // We apply this round up only if  we passed the totalTokensWithoutPriceRise
        if(_totalSupply <= totalTokensWithoutPriceRise) return _totalSupply;
        if(_totalSupply.mod(MIN_WEI_UNIT) != 0) return _totalSupply.add(MIN_WEI_UNIT).sub(_totalSupply.mod(MIN_WEI_UNIT));
        return _totalSupply;
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @param _totalSupply number of tokens to sell
    /// return the number of tokens rounded down to the nearest whole wei hundred
    function roundNbTokensForSelling(uint256 _totalSupply) private view returns(uint256) {
        // We apply this round down only if we passed the totalTokensWithoutPriceRise
        if(_totalSupply <= totalTokensWithoutPriceRise) return _totalSupply;
        return _totalSupply.sub(_totalSupply.mod(MIN_WEI_UNIT));
    }


    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @dev To get the sell price of X tokens you pass the buy price of X tokens and apply the lockedTokenPercentage
    /// @param _buyPrice the buy price we want to apply the lockedTokenPercentage to get the current sell price
    /// @return the price of a token purchase
    function applyRatio(uint _buyPrice) private view returns (uint256) {
        return _buyPrice.mul(uint(100).sub(lockedTokenPercentage)).div(100);
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @dev This is an helper function for getting price of the all supplied tokens or a future hypothetical number of supplied tokens
    /// @dev See getBuyPrice function to check why and how we use it
    /// @dev This function is internal because the IdeaToken uses it in dMarketCap function
    /// @param _totalSupply The number of the all supplied tokens or future hypothetical number of supplied tokens
    /// @return price of a nextTotalSupply purchase from 0 token supplied
    function getBuyPriceOfSupply(uint _totalSupply) internal view returns(uint256) {
        // base price of nextTotalSupply
        uint baseCost = _totalSupply.mul(basePrice);

        // price of tokens in completed tokensInEachInterval minus the applied baseCost
        // We apply Arithmetic Progression sum, this is the simplification of:
        /*
        uint completedPriceRiseCost = tokensInEachInterval.div(DECIMAL)
            .mul(
                getCompleteSteps(_totalSupply).mul(
                    priceRisePerInterval.mul(2).add(
                        priceRisePerInterval.mul(getCompleteSteps(_totalSupply).sub(1))
                ))
            ).div(2);
        */
        uint completedPriceRiseCost = tokensInEachInterval
            .mul(priceRisePerInterval)
            .mul((getCompleteSteps(_totalSupply).mul(getCompleteSteps(_totalSupply).add(1))).div(2)).div(DECIMAL);

        // price of remaining tokens minus the applied baseCost
        uint remainingPriceRiseCost = getRemainingTokensInIncompleteStep(_totalSupply)
        .mul(priceRisePerInterval)
        .mul(getCompleteSteps(_totalSupply).add(1)) / (10 ** 18);

        return baseCost.add(completedPriceRiseCost).add(remainingPriceRiseCost);
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @dev For getting the buy price of X tokens we do: price of X tokens = (price of currentSupply + X) - (price of currentSupply)
    /// @param _totalSupply The number of already supplied tokens
    /// @param _nbTokensToBuy The number of tokens to buy
    /// @return price of a nbTokensToBuy purchase
    function getBuyPrice(uint _totalSupply, uint _nbTokensToBuy) public view returns(uint256) {
        require(_nbTokensToBuy > 0, "You can not buy 0 token.");
        uint256 supplyBefore = roundNbTokensForBuying(_totalSupply);
        uint256 supplyAfter = roundNbTokensForBuying(_totalSupply.add(_nbTokensToBuy));
        return getBuyPriceOfSupply(supplyAfter).sub(getBuyPriceOfSupply(supplyBefore));
    }

    /// @author ["Sofiane Akermoun <akersof@gmail.com>"]
    /// @dev For getting the sell price of X tokens we applied the sell ratio to: (price of _totalSupply) - (price of (_totalSupply - X))
    /// @param _totalSupply the number of already supplied tokens
    /// @param _nbTokensToSell The number of tokens to sell
    /// @return price of an amount tokens sale
    function getSellPrice(uint _totalSupply, uint _nbTokensToSell) public view returns(uint256) {
        require(_nbTokensToSell > 0, "You can not sell 0 token");
        require(_totalSupply >= _nbTokensToSell, "You can not sell more than total supplied tokens");
        uint256 supplyBefore = roundNbTokensForSelling(_totalSupply);
        uint256 supplyAfter = roundNbTokensForSelling(_totalSupply.sub(_nbTokensToSell));
        return applyRatio(getBuyPriceOfSupply(supplyBefore).sub(getBuyPriceOfSupply(supplyAfter)));

    }
}
