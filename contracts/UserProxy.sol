pragma solidity ^0.5.8;

import "./openzeppelin/SafeMath.sol";
import "./openzeppelin/IERC20.sol";
import "./rtoken-contracts/IRToken.sol";
import "./dex/IUniswapFactory.sol";
import "./dex/IUniswapExchange.sol";

interface IIdeaToken {
    function transfer(address, uint256) external returns (bool);
    function getCostOfBuyingTokens(uint) external view returns (uint);
}

interface IIdeaTokenFactory {
    function getPublicationContractAddress(string calldata) external view returns (address);
    function getCostOfIdeaToken(string calldata, uint) external view returns (uint);
    function buyIdeaToken(string calldata, uint) external;
}

interface IUserProxyRegistry {
    function getAddresses() external view returns (address, address, address, address);
}

// Allows the user to buy IdeaTokens with a single tx
contract UserProxy {
    using SafeMath for uint256;

    address public owner;
    IUserProxyRegistry public registry;

    modifier onlyOwner {
        require(owner == msg.sender, "Only owner can call this method");
        _;
    }

    event UserProxyCreated(address owner, address registry);
    event IdeaTokensBought(string indexed domain, uint amount);

    function both(bool a, bool b) public pure returns (bool) {
        return a && b;
    }

    function either(bool a, bool b) public pure returns (bool) {
        return a || b;
    }

    constructor(address _owner, address _registry) public {

        owner = _owner;
        registry = IUserProxyRegistry(_registry);

        emit UserProxyCreated(owner, address(registry));
    }

    function getAdressesFromRegistry() internal view returns(IIdeaTokenFactory, IERC20, IRToken, IUniswapFactory) {
        (address factoryAddress, address daiAddress, address rdaiAddress, address uniswapAddress) = registry.getAddresses();
        return (IIdeaTokenFactory(factoryAddress), IERC20(daiAddress), IRToken(rdaiAddress), IUniswapFactory(uniswapAddress));
    }

    function buyIdeaTokens(
        address _inputToken,
        uint _inputAmount,
        uint _minOutputAmount,
        uint _outputAmount,
        string calldata _domain) external payable onlyOwner {

        (IIdeaTokenFactory factory, IERC20 dai, IRToken rdai, IUniswapFactory uniswapFactory) = getAdressesFromRegistry();

        preCheckBuy(_inputToken, _inputAmount, _minOutputAmount, _outputAmount);

        if(_inputToken != address(0)) {
            getTokensFromOwner(_inputToken, _inputAmount);
        }

        uint rDaiAmount = _inputAmount;
        if(_inputToken != address(dai) && _inputToken != address(rdai)) {
            if(_inputToken == address(0)) {
                rDaiAmount = swapEthToToken(address(dai), msg.value, uniswapFactory);
            } else {
                rDaiAmount = swapTokenToToken(_inputToken, address(dai), _inputAmount, uniswapFactory);
            }
        }

        if(_inputToken != address(rdai)) {
            require(dai.approve(address(rdai), rDaiAmount), "dai rdai approve failed");
            require(rdai.mint(rDaiAmount), "rdai mint failed");
        }

        rDaiToIdeaTokens(
            rdai,
            rDaiAmount,
            _minOutputAmount,
            _outputAmount,
            factory,
            _domain,
            owner
        );

         rdai.transferAll(owner);
    }

    function preCheckBuy(address _inputToken, uint _inputAmount, uint _minOutputAmount, uint _outputAmount) private {
        require(
            either(
                both(_inputToken == address(0), msg.value != 0),
                both(_inputToken != address(0), msg.value == 0)
            ),
            "invalid input token and value combination"
        );

        require(either(_inputAmount != 0, _inputToken == address(0)), "invalid input amount");
        require(_minOutputAmount != 0, "min output amount not set");
        require(_outputAmount >= _minOutputAmount, "invalid output amount");
    }

    function getTokensFromOwner(address _token, uint _amount) private {
        require(IERC20(_token).transferFrom(owner, address(this), _amount), "failed to get tokens from owner");
    }

    function swapEthToToken(address _toToken, uint256 _value, IUniswapFactory _uniswap) private returns (uint) {
        IUniswapExchange exchange = IUniswapExchange(_uniswap.getExchange(_toToken));
        require(address(exchange) != address(0), "exchange for dai does not exist");

        return exchange.ethToTokenSwapInput.value(_value)(
            1,                  // min token output, not required
            now                 // deadline
        );
    }

    function swapTokenToToken(address _fromToken, address _toToken, uint256 _amount, IUniswapFactory _uniswap) private returns (uint) {
        IUniswapExchange exchange = IUniswapExchange(_uniswap.getExchange(_fromToken));
        require(address(exchange) != address(0), "exchange for token does not exist");
        require(IERC20(_fromToken).approve(address(exchange), _amount), "input token uniswap approve failed");

        return exchange.tokenToTokenSwapInput(
            _amount,   // input amount
            1,          // min token output, not required
            1,          // min wei bought, not required
            now,        // deadline
            _toToken   // output token
        );
    }

    function rDaiToIdeaTokens(
        IRToken _rDai,
        uint _rDaiAmount,
        uint _minOutputAmount,
        uint _outputAmount,
        IIdeaTokenFactory _factory,
        string memory _domain,
        address _recipient) private {

        IIdeaToken ideaToken = IIdeaToken(_factory.getPublicationContractAddress(_domain));
        require(address(ideaToken) != address(0), "invalid domain");

        uint buyAmount;
        if(_rDaiAmount >= ideaToken.getCostOfBuyingTokens(_outputAmount)) {
            buyAmount = _outputAmount;
        } else if(_rDaiAmount >= ideaToken.getCostOfBuyingTokens(_minOutputAmount)) {
            buyAmount = _minOutputAmount;
        } else {
            revert("slippage too high");
        }

        require(_rDai.approve(address(_factory), _rDaiAmount), "rdai factory approve failed");
        _factory.buyIdeaToken(_domain, buyAmount);

        require(ideaToken.transfer(_recipient, buyAmount), "idea token transfer failed");
        emit IdeaTokensBought(_domain, buyAmount);
    }
}