pragma solidity ^0.5.8;

import "../openzeppelin/Ownable.sol";
import "../openzeppelin/IERC20.sol";

import "../rtoken-contracts/IRToken.sol";
import "../rtoken-contracts/IAllocationStrategy.sol";

import "./MockDai.sol";

contract MockAllocationStrategy is IAllocationStrategy {

    MockDai private token;
    uint private exchrate;

    constructor(MockDai token_) public {
      token = token_;
      exchrate = 1;
    }

    /// @dev ISavingStrategy.underlying implementation
    function underlying() external view returns (address) {
        return address(token);
    }

    /// @dev ISavingStrategy.exchangeRateStored implementation
    function exchangeRateStored() external view returns (uint256) {
        return exchrate;
    }

    /// @dev ISavingStrategy.accrueInterest implementation
    function accrueInterest() external returns (bool) {
        return true;
    }

    function setExchangeRate(uint _exchrate) external {
        exchrate = _exchrate;
    }

    /// @dev ISavingStrategy.investUnderlying implementation
    function investUnderlying(uint256 investAmount) external returns (uint256) {
      require(token.balanceOf(msg.sender) >= investAmount, "Sender does not have enough tokens to invest");
      token.transferFrom(msg.sender, address(0x000000000000000000000000000000000000dEaD), investAmount);
      // The below line has no use. At the end, transaction object will be returned
      return investAmount * exchrate;
    }

    /// @dev ISavingStrategy.redeemUnderlying implementation
    function redeemUnderlying(uint256 redeemAmount) external returns (uint256) {
      token.mint(msg.sender, redeemAmount);
      // The below line has no use. At the end, transaction object will be returned
      return redeemAmount;
    }

    /// @dev ISavingStrategy.redeemAll implementation
    function redeemAll() external returns (uint256 savingsAmount, uint256 underlyingAmount) {
      savingsAmount = 0;
      underlyingAmount = 0;
      token.transfer(msg.sender, underlyingAmount);
    }
}
