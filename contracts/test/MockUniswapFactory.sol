/*
    Simplified implementation of the Uniswap Vyper contracts.
    See https://github.com/Uniswap/contracts-vyper
*/

pragma solidity ^0.5.8;

import "../openzeppelin/SafeMath.sol";
import "./MockUniswapExchange.sol";

contract MockUniswapFactory {
    using SafeMath for uint256;

    uint256 public tokenCount;
    mapping(address => address) private tokenToExchange;
    mapping(address => address) private exchangeToToken;
    mapping(uint => address) private idToToken;

    event NewExchange(address token, address exchange);

    function createExchange(address _token) public returns (address) {
        require(_token != address(0), "token must not be zero address");
        require(tokenToExchange[_token] == address(0), "exchange already registered");

        address exchange = address(new MockUniswapExchange(_token));

        tokenToExchange[_token] = exchange;
        exchangeToToken[exchange] = _token;
        tokenCount = tokenCount.add(1);
        idToToken[tokenCount] = _token;

        emit NewExchange(_token, exchange);

        return exchange;
    }

    function getExchange(address _token) public view returns(address) {
        return tokenToExchange[_token];
    }

    function getToken(address _exchange) public view returns(address) {
        return exchangeToToken[_exchange];
    }

    function getTokenWithId(uint _tokenId) public view returns (address) {
        return idToToken[_tokenId];
    }
}