/*
    Simplified implementation of the Uniswap Vyper contracts.
    See https://github.com/Uniswap/contracts-vyper
*/

pragma solidity ^0.5.8;

import "../openzeppelin/SafeMath.sol";
import "../openzeppelin/ERC20.sol";

interface IMockUniswapFactory {
    function getExchange(address) external view returns (address);
}

contract MockUniswapExchange is ERC20 {
    using SafeMath for uint256;

    bytes32 public name; // Uniswap V1
    bytes32 public symbol; // UNI-V1
    uint256 public decimals;

    ERC20 private token;
    IMockUniswapFactory private factory;

    event AddLiquidity(address provider, uint256 ethAmount, uint256 tokenAmount);
    event RemoveLiquidity(address provider, uint256 ethAmount, uint256 tokenAmount);
    event TokenPurchase(address buyer, uint ethSold, uint tokensBought);
    event EthPurchase(address buyer, uint tokensSold, uint ethBought);

    constructor(address _token) public {
        require(_token != address(0), "token must not be zero address");

        factory = IMockUniswapFactory(msg.sender);
        token = ERC20(_token);
        name = 0x556e697377617020563100000000000000000000000000000000000000000000;
        symbol = 0x554e492d56310000000000000000000000000000000000000000000000000000;
        decimals = 18;
    }

    function addLiquidity(uint _minLiquidity, uint _maxTokens, uint _deadline) public payable returns (uint) {
        require(_deadline >= now, "deadline over");
        require(_maxTokens > 0, "max tokens must not be zero");

        uint supply = totalSupply();

        if(supply > 0) {
            require(_minLiquidity > 0, "minLiquidity must not be zero");
            require(address(this).balance >= msg.value, "cant add more than current balance");

            uint ethReserve = address(this).balance - msg.value;
            uint tokenReserve = token.balanceOf(address(this));
            uint tokenAmount = msg.value.mul(tokenReserve).div(ethReserve).add(1);
            uint liquidityMinted = msg.value.mul(supply).div(ethReserve);
            require(_maxTokens >= tokenAmount, "maxTokens must be >= tokenAmount");
            require(liquidityMinted >= _minLiquidity, "liquidityMinted must be >= minLiquditiy");
            _mint(msg.sender, liquidityMinted);
            require(token.transferFrom(msg.sender, address(this), tokenAmount), "could not transfer tokens");

            emit AddLiquidity(msg.sender, msg.value, tokenAmount);

            return liquidityMinted;
        } else {
            require(msg.value >= 1000000000, "not enough value");

            uint initialLiquidity = address(this).balance;
            _mint(msg.sender, initialLiquidity);
            require(token.transferFrom(msg.sender, address(this), _maxTokens), "could not transfer tokens");

            emit AddLiquidity(msg.sender, msg.value, _maxTokens);

            return initialLiquidity;
        }
    }

    function removeLiquidity(uint _amount, uint _minEth, uint _minTokens, uint _deadline) public returns (uint, uint) {
        require(_deadline >= now, "deadline over");
        require(_amount > 0, "amount must be greater than zero");
        require(_minEth > 0, "minEth must be greater than zero");
        require(_minTokens > 0, "minTokens must be greater than zero");

        uint totalLiquidity = totalSupply();
        require(totalLiquidity > 0, "no liquidity available");

        uint tokenReserve = token.balanceOf(address(this));
        uint ethAmount = _amount.mul(address(this).balance).div(totalLiquidity);
        uint tokenAmount = _amount.mul(tokenReserve).div(totalLiquidity);
        require(ethAmount >= _minEth, "not enough eth available");
        require(tokenAmount >= _minTokens, "not enough tokens available");
        _burn(msg.sender, _amount);
        msg.sender.transfer(ethAmount);
        require(token.transfer(msg.sender, tokenAmount), "token transfer failed");

        emit RemoveLiquidity(msg.sender, ethAmount, tokenAmount);

        return (ethAmount, tokenAmount);
    }

    function getInputPrice(uint _inputAmount, uint _inputReserve, uint _outputReserve) public pure returns (uint256) {
        require(_inputReserve > 0, "input reserve must be greater than zero");
        require(_outputReserve > 0, "output reserve must be greater than zero");

        uint inputAmountWithFee = _inputAmount.mul(997);
        uint numerator = inputAmountWithFee.mul(_outputReserve);
        uint denominator = _inputReserve.mul(1000).add(inputAmountWithFee);

        return numerator.div(denominator);
    }

    function ethToTokenInput(uint _ethSold, uint _minTokens, uint _deadline, address _buyer, address _recipient) private returns (uint) {
        require(_deadline >= now, "deadline over");
        require(_ethSold > 0, "ethSold must be greater than zero");
        require(_minTokens > 0, "minTokens must be greater than zero");

        uint tokenReserve = token.balanceOf(address(this));
        uint tokensBought = getInputPrice(_ethSold, address(this).balance.sub(_ethSold), tokenReserve);
        require(tokensBought >= _minTokens, "not enough tokens available");
        require(token.transfer(_recipient, tokensBought), "token transfer failed");

        emit TokenPurchase(_buyer, _ethSold, tokensBought);

        return tokensBought;
    }

    function tokenToTokenInput(uint _tokensSold, uint _minTokensBought, uint _minEthBought, uint _deadline, address _buyer,
    address _recipient, address _exchangeAddr) private returns (uint) {
        require(_deadline >= now, "deadline over");
        require(_tokensSold > 0, "tokensSold must be greater than zero");
        require(_minTokensBought > 0, "minTokensBought must be greater than zero");
        require(_minEthBought > 0, "minEthBought must be greater than zero");
        require(address(this) != _exchangeAddr, "exchange address cant be this");
        require(_exchangeAddr != address(0), "exchange address cant be zero address");

        uint tokenReserve = token.balanceOf(address(this));
        uint ethBought = getInputPrice(_tokensSold, tokenReserve, address(this).balance);
        require(ethBought >= _minEthBought, "not enough eth available");
        require(token.transferFrom(_buyer, address(this), _tokensSold), "token transfer failed");
        uint tokensBought = MockUniswapExchange(_exchangeAddr).ethToTokenTransferInput.value(ethBought)(_minTokensBought, _deadline, _recipient);

        emit EthPurchase(_buyer, _tokensSold, ethBought);

        return tokensBought;
    }

    function ethToTokenSwapInput(uint _minTokens, uint _deadline) public payable returns (uint) {
        return ethToTokenInput(msg.value, _minTokens, _deadline, msg.sender, msg.sender);
    }

    function ethToTokenTransferInput(uint _minTokens, uint _deadline, address recipient) public payable returns (uint) {
        return ethToTokenInput(msg.value, _minTokens, _deadline, msg.sender, recipient);
    }

    function tokenToTokenSwapInput(uint _tokensSold, uint _minTokensBought, uint _minEthBought, uint _deadline, address _tokenAddr)
    public returns (uint) {
        address exchangeAddr = factory.getExchange(_tokenAddr);
        return tokenToTokenInput(_tokensSold, _minTokensBought, _minEthBought, _deadline, msg.sender, msg.sender, exchangeAddr);
    }
}