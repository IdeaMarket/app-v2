pragma solidity ^0.5.8;

import "../openzeppelin/ERC20.sol";

contract MockDai is ERC20 {
    string public name = "MockDAI";
    string public symbol = "MockDAI";
    uint8 public decimals = 18;

    function mint(address addr, uint amount) public {
        _mint(addr, amount);
    }

    function transferFrom(address sender, address recipient, uint256 amount) public returns (bool) {
      _transfer(sender, recipient, amount);
      uint256 allowance = allowance(sender, msg.sender);
      if(allowance >= amount) {
        _approve(sender, msg.sender, allowance.sub(amount));
      }
      return true;
    }
}
