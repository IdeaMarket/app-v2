pragma solidity ^0.5.8;

import "./UserProxy.sol";


// Simple proxy registry to deploy and keep track of users proxies
contract UserProxyRegistry {

    address public owner;
    address public factory;
    address public dai;
    address public rdai;
    address public uniswapFactory;

    mapping(address => address) private proxyAddresses;

    event ProxyAddressUpdated(address user, address oldAddress, address newAddress);
    event OwnershipChanged(address oldOwner, address newOwner);
    event FactoryAddressChanged(address oldFactoryAddress, address newFactoryAddress);
    event DaiAddressChanged(address oldDaiAddress, address newDaiAddress);
    event rDaiAddressChanged(address oldRDaiAddress, address newRDaiAddress);
    event UniswapFactoryChanged(address oldUniswapAddress, address newUniswapAddress);

    modifier onlyOwner {
        require(msg.sender == owner, "Only owner can call this method");
        _;
    }

    constructor(address _factory, address _dai, address _rdai, address _uniswapFactory) public {
        owner = msg.sender;
        factory = _factory;
        dai = _dai;
        rdai = _rdai;
        uniswapFactory = _uniswapFactory;
    }


    // Deploys a proxy on behalf of the caller and stores its address
    function deployProxy() external returns (address) {
        address oldAddr = proxyAddresses[msg.sender];
        address newAddr = address(new UserProxy(msg.sender, address(this)));
        proxyAddresses[msg.sender] = newAddr;

        emit ProxyAddressUpdated(msg.sender, oldAddr, newAddr);

        return newAddr;
    }

    function changeOwner(address _newOwner) external onlyOwner {
        address oldOwner = owner;
        owner = _newOwner;
        emit OwnershipChanged(oldOwner, _newOwner);
    }

    function changeFactoryAddress(address _newFactory) external onlyOwner {
        address oldFactory = factory;
        factory = _newFactory;
        emit FactoryAddressChanged(oldFactory, _newFactory);
    }

    function changeDaiAddress(address _newDaiAddress) external onlyOwner {
        address oldDaiAddress = dai;
        dai = _newDaiAddress;
        emit DaiAddressChanged(oldDaiAddress, _newDaiAddress);
    }

    function changeRDaiAddress(address _newRDaiAddress) external onlyOwner {
        address oldRDaiAddress = rdai;
        rdai = _newRDaiAddress;
        emit rDaiAddressChanged(oldRDaiAddress, _newRDaiAddress);
    }

    function changeUniswapFactoryAddress(address _newUniswapFactoryAddress) external onlyOwner {
        address oldUniswapFactoryAddress = uniswapFactory;
        uniswapFactory = _newUniswapFactoryAddress;
        emit rDaiAddressChanged(oldUniswapFactoryAddress, _newUniswapFactoryAddress);
    }

    function getAddresses() external view returns (address, address, address, address) {
        return(factory, dai, rdai, uniswapFactory);
    }

    function getUserProxyAddress(address _user) external view returns(address) {
        return proxyAddresses[_user];
    }
}