pragma solidity ^0.5.8;

//import "./IdeaTokenFactory.sol";
import "./openzeppelin/SafeMath.sol";
import "./openzeppelin/ERC20.sol";
import "./openzeppelin/ReentrancyGuard.sol";
import "./rtoken-contracts/IRToken.sol";
import "./IdeaTokenBondingCurve.sol";

contract IdeaToken is ReentrancyGuard, ERC20, IdeaTokenBondingCurve {
    using SafeMath for uint256;

    address public baseContract;
    uint public domainId;
    string public domainStrId;

    string public name;

    string public symbol;

    uint8 public decimals = 18;

    uint lockedTokenPercentage;

    uint256 public marketCap;
    uint256 public totalDeposit;
    uint256 public burned_amt;

    address payable rdai_contract_address;

    modifier onlyBaseContract {
        require(msg.sender == baseContract, "This method can only be called by the base contract");
        _;
    }

    constructor(address _baseContract,
                address payable _rdai_contract_address,
                address devFund,
                uint _domainId,
                string memory _domainStrId,
                uint _tokensInEachInterval,
                uint _priceRisePerInterval,
                uint _basePrice,
                uint _totalTokensWithoutPriceRise)
        IdeaTokenBondingCurve(_tokensInEachInterval, _priceRisePerInterval, _basePrice, _totalTokensWithoutPriceRise, 2)
    public {
        baseContract = _baseContract;
        domainId = _domainId;
        domainStrId = _domainStrId;
        rdai_contract_address = _rdai_contract_address;
        name = domainStrId;
        symbol = domainStrId;

        lockedTokenPercentage = 2; // if current buy price is 100 then current sell price would be ~98

        address[] memory hataddrs = new address[](1);
        uint32[] memory hatdistr = new uint32[](1);

        hataddrs[0] = _baseContract;
        hatdistr[0] = uint32(100);

        IRToken(rdai_contract_address).createHat(hataddrs, hatdistr, true);
    }

    function applyInterestMultiplier(uint amount) public view returns (uint) {
        require(rDaiBalance() >= totalDeposit, "rDai balance should always be >= totalDeposit");

        if (totalDeposit == 0) {
            return amount;
        }
        return amount.mul(rDaiBalance().div(totalDeposit));
    }

    function getCostOfBuyingTokens(uint _qty) public view returns (uint) {
        return applyInterestMultiplier(getBuyPrice(totalSupply(), _qty));
    }

    function rDaiTransfer(address to, uint amount) public onlyBaseContract {
        IERC20 rdai_contract = IERC20(rdai_contract_address);
        rdai_contract.transfer(to, amount);
    }

    function getBasePriceofSellingTokens(uint _qty) public view returns (uint) {
        return getSellPrice(totalSupply(), _qty);
    }

    function getPriceofSellingTokens(uint _qty) public view returns (uint) {
        return applyInterestMultiplier(getBasePriceofSellingTokens(_qty));
    }

    function buyTokens(uint _qty,address _buyer, uint payAmt) public nonReentrant onlyBaseContract {
        uint supply = totalSupply();
        marketCap = getBuyPriceOfSupply(supply + _qty);
        totalDeposit = totalDeposit.add(payAmt);
        _mint(_buyer, _qty);
    }

    function sellTokens(uint _qty,address _buyer, uint payAmt) public nonReentrant onlyBaseContract {
        uint supply = totalSupply();
        marketCap = getBuyPriceOfSupply(supply.sub(_qty));
        totalDeposit = totalDeposit.sub(payAmt * 100 / (100-lockedTokenPercentage));
        burned_amt = burned_amt + (payAmt * (lockedTokenPercentage) / (100-lockedTokenPercentage));
        _burn(_buyer, _qty);
    }

    function rDaiBalance() public view returns (uint256) {
        return IRToken(rdai_contract_address).balanceOf(address(this)).sub(burned_amt);
    }

    function changeBaseContract(address _newBaseContract) external onlyBaseContract {
        baseContract = _newBaseContract;
    }

}
