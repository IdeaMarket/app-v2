const AppLayout = import("layouts/AppLayout");
const Index = import("pages/Index");
const About = import("pages/About");
const Wallet = import("pages/Wallet");
const Error404 = import("pages/Error404");

const routes = [
  {
    path: "/",
    component: () => AppLayout,
    children: [
      { path: "", component: () => Index },
      {
        path: "about",
        component: () => About
      },
      {
        path: "wallet",
        component: () => Wallet
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => Error404
  });
}

export default routes;
