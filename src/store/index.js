import Vue from "vue";
import Vuex from "vuex";
import publications from "./publications";
import users from "./users";

Vue.use(Vuex);

export default function(/* { ssrContext } */) {
  const store = new Vuex.Store({
    modules: {
      publications,
      users
    }
  });

  return store;
}
