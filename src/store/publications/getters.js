export function all(state) {
  return state.publications;
}

export function mine(state) {
  return state.publications.filter(publication => publication.owned === true);
}

export function user(state) {
  return state.publications.web3;
}

export function marketCap(state) {
  return state.marketCap;
}

export function permafundBalance(state) {
  return state.permafundBalance;
}

export function getDaiBalance(state) {
  return state.daiBalance;
}
