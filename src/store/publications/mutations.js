import Vue from "vue";

/**
 * Add publication to state.
 */
export function add(state, publication) {
  state.publications.push(publication);
}

export function web3(state, web3) {
  state.web3 = web3.web3;
  state.web3.address = web3.address;
}

export function web3Connect(state, { web3Connect }) {
  state.web3Connect = web3Connect;
}

export function update(state, updated) {
  const index = state.publications.findIndex(
    publication => publication.address === updated.address
  );
  Vue.set(state.publications, index, updated);
}

export function toggleCreateDialog(state, isOpen) {
  if (typeof isOpen === "boolean") {
    state.createDialog = isOpen;
  } else {
    state.createDialog = !state.createDialog;
  }
}

export function toggleBuyDialog(state, isOpen) {
  if (typeof isOpen === "boolean") {
    state.buyDialog = isOpen;
  } else {
    state.buyDialog = !state.buyDialog;
  }
}

export function toggleSellDialog(state, isOpen) {
  if (typeof isOpen === "boolean") {
    state.sellDialog = isOpen;
  } else {
    state.sellDialog = !state.sellDialog;
  }
}

export function setBuyingDomain(state, domain) {
  state.buyingDomain = domain;
}

export function setSellingDomain(state, { domain, amount }) {
  state.sellingDomain = domain;
  state.sellingTokenAmount = amount;
}

export function buyTokens(state, payload) {
  const publication = state.publications.find(
    publication => publication.domain === payload.domain
  );
  publication.marketCap = Number(payload.tokenCap);

  publication.amount += Number(payload.amount);

  publication.price = Number(payload.tokenCost);

  state.marketCap = Number(payload.factoryCap);
}

export function sellTokens(state, payload) {
  const publication = state.publications.find(
    publication => publication.domain === payload.domain
  );
  publication.marketCap = Number(payload.tokenCap);

  publication.amount -= Number(payload.amount);

  publication.price = Number(payload.tokenCost);

  state.marketCap = Number(payload.factoryCap);
}

export function addToMarketCap(state, payload) {
  state.marketCap += Number(payload.amount);
}

export function addToPermafundBalance(state, payload) {
  state.permafundBalance += Number(payload.burned);
}

export function setDaiBalance(state, payload) {
  state.daiBalance = payload.balance;
}
