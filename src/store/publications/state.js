export default {
  publications: [],
  createDialog: false,
  web3: {},
  web3Connect: {},
  buyDialog: false,
  sellDialog: false,
  buyingDomain: "",
  sellingDomain: "",
  sellingTokenAmount: "",
  marketCap: 0,
  permafundBalance: 0,
  daiBalance: 0
};
