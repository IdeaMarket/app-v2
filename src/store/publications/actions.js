// import { web3 } from "boot/web3";
import Web3Connect from "web3connect";
import Portis from "@portis/web3";
import Web3 from "web3";
import { Notify } from "quasar";
import {
  TRADE_EXACT,
  BigNumber,
  getTradeDetails,
  getTokenReserves,
  getMarketDetails
} from "@uniswap/sdk";

import contract from "truffle-contract";

import IdeaTokenFactoryABI from "src/contracts/IdeaTokenFactory";
import IdeaTokenABI from "src/contracts/IdeaToken";
import DSTokenABI from "src/abis/DSToken";
import RTokenABI from "src/abis/RToken";
import UserProxyRegistryABI from "src/contracts/UserProxyRegistry";
import UserProxyABI from "src/contracts/UserProxy";
import IERC20ABI from "src/contracts/IERC20";

let web3;

const IdeaTokenFactory = contract(IdeaTokenFactoryABI);
const IdeaToken = contract(IdeaTokenABI);
const DSToken = contract(DSTokenABI);
const RToken = contract(RTokenABI);
const UserProxyRegistry = contract(UserProxyRegistryABI);
const UserProxy = contract(UserProxyABI);
const IERC20 = contract(IERC20ABI);

const daiMainnetAddress = "0x6b175474e89094c44da98b954eedeac495271d0f";

export async function initWeb3(context) {
  let web3Connect = new Web3Connect.Core({
    providerOptions: {
      portis: {
        package: Portis,
        options: {
          id: "39586349-2e8b-4c4c-9520-d4f23ccb4541"
        }
      }
    }
  });

  context.commit("web3Connect", { web3Connect });

  await web3Connect.on("connect", async provider => {
    web3 = new Web3(provider);
    let accounts = await web3.eth.getAccounts();
    await IdeaTokenFactory.setProvider(web3.currentProvider);
    await IdeaToken.setProvider(web3.currentProvider);
    await DSToken.setProvider(web3.currentProvider);
    await RToken.setProvider(web3.currentProvider);
    await UserProxyRegistry.setProvider(web3.currentProvider);
    await UserProxy.setProvider(web3.currentProvider);
    await IERC20.setProvider(web3.currentProvider);
    context.commit("web3", { web3, address: accounts[0] });
    await context.dispatch("initialize");
  });
  await web3Connect.toggleModal();
}

export async function initialize(context) {
  const factory = await IdeaTokenFactory.deployed();
  const addresses = await factory.getPublications();

  const [account] = await web3.eth.getAccounts();
  const dai = await DSToken.at(daiMainnetAddress);
  const balance = web3.utils.fromWei((await dai.balanceOf(account)).toString());
  context.commit("setDaiBalance", { balance });

  addresses.forEach(async address => {
    const token = await IdeaToken.at(address);
    const domain = await token.domainStrId();
    const tokenCap = web3.utils.fromWei((await token.marketCap()).toString());
    const burned = web3.utils.fromWei((await token.burned_amt()).toString());

    context.commit("add", {
      address,
      domain,
      price: web3.utils.fromWei(
        (await factory.getCostOfIdeaToken(
          domain,
          web3.utils.toWei("1")
        )).toString()
      ),
      marketCap: tokenCap.toString()
      // interestAccrued:
      //   (await token.rDaiBalance()) > (await token.totalDeposit())
      // change: "+4.9",
      // color: "positive"
    });

    context.commit("addToMarketCap", { amount: tokenCap });
    context.commit("addToPermafundBalance", { burned });
    await context.dispatch("initializeWallet");
  });
}

export async function initializeWallet(context) {
  const factory = await IdeaTokenFactory.deployed();
  const [account] = await web3.eth.getAccounts();
  const publications = context.state.publications;

  publications.forEach(async publication => {
    const token = await IdeaToken.at(publication.address);
    const domain = await token.domainStrId();

    // User holdings
    const amount = await token.balanceOf(account);
    const marketCap = await token.marketCap();
    let owned = false;
    let interest = "0";
    let value = "0";

    if (amount > 0) {
      owned = true;
      value = await token.getPriceofSellingTokens(amount.toString());

      // Calculate interest
      const basePrice = await token.getBasePriceofSellingTokens(
        amount.toString()
      );
      const rDaiBalance = await token.rDaiBalance();
      const totalDeposit = await token.totalDeposit();
      interest = Math.round(
        (basePrice * (rDaiBalance - totalDeposit)) / totalDeposit
      );
    }

    context.commit("update", {
      address: publication.address,
      domain,
      price: web3.utils.fromWei(
        (await factory.getCostOfIdeaToken(
          domain,
          web3.utils.toWei("1")
        )).toString()
      ),
      marketCap: web3.utils.fromWei(marketCap.toString()),
      owned,
      value: web3.utils.fromWei(value.toString()),
      amount: web3.utils.fromWei(amount.toString()),
      interest: web3.utils.fromWei(interest.toString())
    });
  });
}

export async function create(context, domain) {
  if (typeof web3 === "undefined") {
    Notify.create(
      "Please connect to a web3 wallet to view the market. We recommend Portis for beginners."
    );
    await context.state.web3Connect.toggleModal();
    if (typeof web3 === "undefined") {
      Notify.create(
        "Please connect to a web3 wallet to view the market. We recommend Portis for beginners."
      );
    }
  }

  const factory = await IdeaTokenFactory.deployed();
  const [account] = await web3.eth.getAccounts();

  const receipt = await factory.createIdeaToken(domain, {
    from: account,
    gas: "2000000",
    gasPrice: "1250000000"
  });

  const token = await IdeaToken.at(receipt.logs[0].args.newContractAddress);

  context.commit("add", {
    address: token.address,
    rank: (await factory.getPublications()).length,
    domain,
    price: web3.utils.fromWei(
      (await factory.getCostOfIdeaToken(
        domain,
        web3.utils.toWei("1")
      )).toString()
    ),
    marketCap: web3.utils.fromWei((await token.marketCap()).toString())
    // change: "+4.9",
    // color: "positive"
  });

  return token.address;
}

export async function buyTokens(context, payload) {
  const [account] = await web3.eth.getAccounts();

  const proxyRegistry = await UserProxyRegistry.deployed();
  let userProxyAddress = (await proxyRegistry.getUserProxyAddress.call(
    account
  )).toString();

  if (userProxyAddress == "0x0000000000000000000000000000000000000000") {
    context.commit("users/toggleAccountDialog", null, { root: true });
  }
  const userProxy = await UserProxy.at(userProxyAddress);

  // Specify the base in toString(). This way it wont be in exp. notation which messes with web3 BN.
  const cost = new web3.utils.BN(
    new BigNumber(payload.cost)
      .multipliedBy(10 ** payload.token.decimals)
      .toString(10)
  );

  if (payload.token.symbol != "ETH") {
    const token = await IERC20.at(payload.token.address);
    const allowance = await token.allowance.call(account, userProxy.address);

    // Proxy doesnt have enough allowance for this token
    if (allowance.lt(cost)) {
      const uint256max = new web3.utils.BN("2")
        .pow(new web3.utils.BN("256"))
        .sub(new web3.utils.BN("1"));
      await token.approve(userProxy.address, uint256max);
    }
  }

  const amount = new web3.utils.BN(web3.utils.toWei(payload.amount));
  const minOutputTokens = new web3.utils.BN(
    web3.utils.toWei(
      ((1.0 - payload.maxSlippage * 0.01) * payload.amount).toString()
    )
  );
  let value;

  if (payload.token.symbol != "ETH") {
    value = new web3.utils.BN("0");
  } else {
    value = cost;
  }

  console.log(cost.toString(), "cost");
  console.log(minOutputTokens.toString(), "minOutputTokens");
  console.log(amount.toString(), "amount");
  console.log(value.toString(), "value");

  await userProxy.buyIdeaTokens(
    payload.token.address,
    cost,
    minOutputTokens,
    amount,
    payload.domain,
    {
      from: account,
      value: value,
      gas: "2250000",
      gasPrice: "1250000000"
    }
  );

  // Get market caps after buying
  const factory = await IdeaTokenFactory.deployed();
  const factoryCap = web3.utils.fromWei((await factory.marketCap()).toString());
  const tokenAddress = await factory.getPublicationContractAddress(
    payload.domain
  );
  const token = await IdeaToken.at(tokenAddress);
  const tokenCap = web3.utils.fromWei((await token.marketCap()).toString());
  const tokenCost = web3.utils.fromWei(
    (await token.getCostOfBuyingTokens(web3.utils.toWei("1"))).toString()
  );

  context.commit("buyTokens", { ...payload, factoryCap, tokenCap, tokenCost });
}

export async function sellTokens(context, payload) {
  const factory = await IdeaTokenFactory.deployed();
  const [account] = await web3.eth.getAccounts();

  await factory.sellIdeaToken(
    payload.domain,
    web3.utils.toWei(payload.amount),
    {
      from: account,
      gas: "825000",
      gasPrice: "1250000000"
    }
  );

  const address = await factory.getPublicationContractAddress(payload.domain);
  const token = await IdeaToken.at(address);
  const burned = web3.utils.fromWei((await token.burned_amt()).toString());

  // Get market caps after buying
  const factoryCap = web3.utils.fromWei((await factory.marketCap()).toString());
  const tokenCap = web3.utils.fromWei((await token.marketCap()).toString());
  const tokenCost = web3.utils.fromWei(
    (await token.getCostOfBuyingTokens(web3.utils.toWei("1"))).toString()
  );

  context.commit("sellTokens", { ...payload, factoryCap, tokenCap, tokenCost });
  context.commit("addToPermafundBalance", { burned });
}

export async function distributeInterest() {
  const factory = await IdeaTokenFactory.deployed();
  const [account] = await web3.eth.getAccounts();
  await factory.distributeInterest({
    from: account,
    gas: "6500000",
    gasPrice: "1250000000"
  });
}

export async function getBalance(context, payload) {
  const [account] = await web3.eth.getAccounts();

  if (payload.token.symbol == "ETH") {
    const wei = await web3.eth.getBalance(account);
    return web3.utils.fromWei(wei).toString();
  }

  const token = await IERC20.at(payload.token.address);
  const balance = new BigNumber(
    (await token.balanceOf.call(account)).toString()
  );
  return balance
    .dividedBy(new BigNumber(10).pow(new BigNumber(payload.token.decimals)))
    .toString();
}

export async function getCost(context, payload) {
  const factory = await IdeaTokenFactory.deployed();
  const costInDai = web3.utils.fromWei(
    (await factory.getCostOfIdeaToken(
      payload.domain,
      web3.utils.toWei(payload.amount)
    )).toString()
  );

  if (payload.token.symbol == "DAI") {
    return costInDai;
  }

  const daiReserves = await getTokenReserves(daiMainnetAddress);
  let inputReserves;
  let tokenDecimals;
  if (payload.token.symbol == "ETH") {
    inputReserves = undefined;
    tokenDecimals = 18;
  } else {
    inputReserves = await getTokenReserves(payload.token.address);
    tokenDecimals = payload.token.decimals;
  }

  const marketDetails = await getMarketDetails(inputReserves, daiReserves);
  const tradeAmount = new BigNumber(costInDai).multipliedBy(10 ** 18);
  const tradeDetails = getTradeDetails(
    TRADE_EXACT.OUTPUT,
    tradeAmount,
    marketDetails
  );
  return tradeDetails.inputAmount.amount.div(10 ** tokenDecimals).toString();
}
