export function toggleAccountDialog(state, isOpen) {
  if (typeof isOpen === "boolean") {
    state.accountDialog = isOpen;
  } else {
    state.accountDialog = !state.accountDialog;
  }
}
