import Web3 from "web3";
import contract from "truffle-contract";
import { Notify } from "quasar";

import UserProxyRegistryABI from "src/contracts/UserProxyRegistry";
const UserProxyRegistry = contract(UserProxyRegistryABI);

import DSTokenABI from "src/abis/DSToken";
const DSToken = contract(DSTokenABI);

const daiMainnetAddress = "0x6b175474e89094c44da98b954eedeac495271d0f";

let web3;

if (window.web3) {
  web3 = new Web3(window.web3.currentProvider);
}

export async function createAccount(context) {
  if (typeof web3 === "undefined") {
    Notify.create(
      "Please connect to a web3 wallet to view the market. We recommend Portis for beginners."
    );
    await context.rootState.publications.web3Connect.toggleModal();
    if (typeof web3 === "undefined") {
      Notify.create(
        "Please connect to a web3 wallet to view the market. We recommend Portis for beginners."
      );
    }
  }

  const [account] = await web3.eth.getAccounts();
  await DSToken.setProvider(web3.currentProvider);
  const dai = await DSToken.at(daiMainnetAddress);

  await UserProxyRegistry.setProvider(web3.currentProvider);
  const proxyRegistry = await UserProxyRegistry.deployed();

  let userProxyAddress = (await proxyRegistry.getUserProxyAddress.call(
    account
  )).toString();

  // Check allowance. This tx might have been cancelled by the user after deploying the proxy.
  const allowance = (await dai.allowance.call(
    account,
    userProxyAddress
  )).toString();

  if (userProxyAddress == "0x0000000000000000000000000000000000000000") {
    const receipt = await proxyRegistry.deployProxy({
      from: account,
      gas: "1250000",
      gasPrice: "2500000000"
    });
    userProxyAddress = receipt.logs[0].args.newAddress;

    await dai.approve(
      userProxyAddress,
      new web3.utils.BN(2)
        .pow(new web3.utils.BN(256))
        .sub(new web3.utils.BN(1)),
      {
        from: account,
        gas: "100000",
        gasPrice: "2500000000"
      }
    );
    // Double check the user's allowance in case the second tx was cancelled.
  } else if (new web3.utils.BN(0).cmp(new web3.utils.BN(allowance)) == 0) {
    await dai.approve(
      userProxyAddress,
      new web3.utils.BN(2)
        .pow(new web3.utils.BN(256))
        .sub(new web3.utils.BN(1)),
      {
        from: account,
        gas: "100000",
        gasPrice: "2500000000"
      }
    );
  } else {
    Notify.create("You already have an account!");
  }
}
