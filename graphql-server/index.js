const IdeaTokenFactoryABI = require("../src/contracts/IdeaTokenFactory");
const IdeaTokenABI = require("../src/contracts/IdeaToken");
const DSTokenABI = require("../src/abis/DSToken");
const RTokenABI = require("../src/abis/RToken");
const Web3 = require("web3");
const contract = require("truffle-contract");

const { GraphQLServer } = require("graphql-yoga");

const IdeaTokenFactory = contract(IdeaTokenFactoryABI);
const IdeaToken = contract(IdeaTokenABI);
const DSToken = contract(DSTokenABI);
const RToken = contract(RTokenABI);

let data = {
  publications: [],
  marketCap: 0
};

const infuraKey = require("../truffle-config").infuraKey

const web3 = new Web3(
  // Replace YOUR-PROJECT-ID with a Project ID from your Infura Dashboard
  new Web3.providers.WebsocketProvider(
    `wss://mainnet.infura.io/ws/v3/${infuraKey}`
  )
);

async function initialize() {
  await IdeaTokenFactory.setProvider(web3.currentProvider);
  await IdeaToken.setProvider(web3.currentProvider);
  await DSToken.setProvider(web3.currentProvider);
  await RToken.setProvider(web3.currentProvider);
  const factory = await IdeaTokenFactory.deployed();
  const addresses = await factory.getPublications();

  addresses.forEach(async (address) => {
    const token = await IdeaToken.at(address);
    const domain = await token.domainStrId();
    const tokenCap = web3.utils.fromWei((await token.marketCap()).toString());
    const totalDeposit = web3.utils.fromWei((await token.totalDeposit()).toString());
    const rDaiBalance = web3.utils.fromWei((await token.rDaiBalance()).toString());
    const burnedAmount = web3.utils.fromWei((await token.burned_amt()).toString());
    data.publications.push({
      address,
      domain,
      price: web3.utils.fromWei(
        (await factory.getCostOfIdeaToken(
          domain,
          web3.utils.toWei("1")
        )).toString()
      ),
      marketCap: tokenCap.toString(),
      totalDeposit: totalDeposit.toString(),
      rDaiBalance: rDaiBalance.toString(),
      burnedAmount: burnedAmount.toString()
    });
    data.publications.sort(
      (a, b) => parseFloat(b.marketCap) - parseFloat(a.marketCap)
    );
    data.publications.forEach(
      (publication, index) => (publication.rank = (index + 1).toString())
    );
    data.marketCap += Number(tokenCap);
  });
}

const typeDefs = `
  type Query {
    publications: [Publication!]!
    totalMarketCap: String!
  }

  type Publication {
    domain: String!
    address: String!
    price: String!
    marketCap: String!
    rank: String!
    totalDeposit: String!
    rDaiBalance: String!
    burnedAmount: String!
  }
`;

const resolvers = {
  Query: {
    totalMarketCap() {
      return data.marketCap;
    },
    publications() {
      return data.publications;
    }
  }
};

const server = new GraphQLServer({ typeDefs, resolvers });
server.start(async () => {
  console.log("Server is running on localhost:4000");
  await initialize();
});
