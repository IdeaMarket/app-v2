const readline = require("readline");

const IdeaTokenFactory = artifacts.require("IdeaTokenFactory");
const UserProxyRegistry = artifacts.require("UserProxyRegistry");
const MockUniswapFactory = artifacts.require("MockUniswapFactory");

function deployITF(deployer, oldFactory, rdaiAddress, devFund) {
  return deployer.deploy(
    IdeaTokenFactory,
    oldFactory, // address _oldFactory,
    rdaiAddress, // address payable _rDai_contract_address,
    devFund, // address _devFund,
    0, // uint256 _minCashOutCoolDown,
    0, // uint256 _additionalNonOwnerCashOutCoolDown,
    1, // uint256 _payoutCapThresholdNum,
    100, // uint256 _payoutCapThresholdDenom,
    0 // uint256 _globalPayoutDust
  );
}

function deployUserProxyRegistry(deployer, factoryAddress, daiAddress, rdaiAddress, uniswapFactoryAddress) {
  return deployer.deploy(
    UserProxyRegistry,
    factoryAddress,
    daiAddress,
    rdaiAddress,
    uniswapFactoryAddress
  );
}

// Doesnt get more nodejs than that
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function readLine(message) {
  return new Promise((resolve, reject) => {
    rl.question(message, (answer) => {
      resolve(answer);
    });
  });
} 


let deployNewFactory;
let transferState;
let oldFactoryAddress;
let deployNewUserProxyRegistry;
let oldUserProxyRegistryAddress;

async function initEnvVariables(network) {

  if (
    network === "development" ||
    network === "gitlab" ||
    network === "coverage"
  ) {
    return true;
  }

  if(network == "") {
    network = "mainnet";
  }

  const envDeployNewFactory = process.env.DEPLOY_NEW_FACTORY;
  const envTransferState = process.env.TRANSFER_STATE;
  oldFactoryAddress = IdeaTokenFactory.address;
  const envDeployNewUserProxyRegistry = process.env.DEPLOY_NEW_USER_PROXY_REGISTRY;
  oldUserProxyRegistryAddress = UserProxyRegistry.address;

  if(envDeployNewFactory == "TRUE") {
    deployNewFactory = true;

    if(envTransferState == "TRUE") {
      transferState = true;
    } else if(envTransferState == "FALSE") {
      transferState = false;
    } else {
      console.error(new Error("Invalid value for env variable TRANSFER_STATE. Expected TRUE OR FALSE, got " + envTransferState));
      return false;
    }

  } else if(envDeployNewFactory == "FALSE") {
    deployNewFactory = false;
    transferState = false;
  } else {
    console.error(new Error("Invalid value for env variable DEPLOY_NEW_FACTORY. Expected TRUE OR FALSE, got " + envDeployNewFactory));
    return false;
  }

  if(envDeployNewUserProxyRegistry == "TRUE") {
    deployNewUserProxyRegistry = true;
  } else if(envDeployNewUserProxyRegistry == "FALSE") {
    deployNewUserProxyRegistry = false;
  } else {
    console.error(new Error("Invalid value for env variable DEPLOY_NEW_USER_PROXY_REGISTRY. Expected TRUE OR FALSE, got " + envDeployNewUserProxyRegistry));
    return false;
  }

  let transferStateOutput = envTransferState;
  if(transferStateOutput == undefined) {
    transferStateOutput = "FALSE";
  } 

  console.log("");
  console.log("******************************* CONFIRM CONFIGURATION ***********************************");
  console.log("*");
  console.log("* Network:\t\t\t\t" + network);
  console.log("* Deploy new IdeaTokenFactory:\t\t" + envDeployNewFactory);
  console.log("* Current IdeaTokenFactory address:\t" + oldFactoryAddress);
  console.log("* Transfer State:\t\t\t" + transferStateOutput);
  console.log("* Deploy new UserProxyRegistry:\t\t" + envDeployNewUserProxyRegistry);
  console.log("* Current UserProxyRegistry address:\t" + oldUserProxyRegistryAddress);
  console.log("*");
  console.log("*****************************************************************************************");
  console.log("");

  let resp; 
  while(resp != "y" && resp != "Y" && resp != "n" && resp != "N") {
    resp = await readLine("Correct? (y/n): ");
    console.log("");
  }

  if(resp != "y" && resp != "Y") {
    console.error(new Error("Aborted by user."));
    return false;
  }

  return true;
}


module.exports = async function(deployer, network, accounts) {
  console.log(network);

  init = await initEnvVariables(network);
  if(!init) {
    throw "Failed to initialize environment variables";
  }

  let daiAddress = "0x6b175474e89094c44da98b954eedeac495271d0f";
  let rdaiAddress = "0x261b45D85cCFeAbb11F022eBa346ee8D1cd488c0";
  let uniswapFactoryAddress = "0xc0a47dFe034B400B47bDaD5FecDa2621de6c4d95";
  // TODO: actual dev fund
  let devFund = "0x04Ff24049E2036194A9755084CaDF147BB36B784";

  // Kovan
  let kovanDaiAddress = "0x4f96fe3b7a6cf9725f59d353f723c1bdb64ca6aa";
  let kovanrDaiAddress = "0xea718e4602125407fafcb721b7d760ad9652dfe7";

  if (
    network === "development" ||
    network === "gitlab" ||
    network === "coverage"
  ) {
    const MockDai = artifacts.require("test/MockDai");
    const MockAllocationStrategy = artifacts.require(
      "test/MockAllocationStrategy"
    );
    const MockUniswapFactory = artifacts.require("test/MockUniswapFactory");
    const RToken = artifacts.require("test/RToken");

    return deployer.deploy(MockDai).then(() => {
      MockDai.deployed().then(mockDai => {
        mockDai.mint(accounts[0], web3.utils.toWei("100000"));
      });
      return deployer
        .deploy(MockAllocationStrategy, MockDai.address)
        .then(() => {
          return deployer
            .deploy(RToken, MockAllocationStrategy.address)
            .then(() => {
              return deployITF(
                deployer,
                "0x0000000000000000000000000000000000000000",
                RToken.address,
                devFund
              ).then(() => {
                return deployer.deploy(MockUniswapFactory)
                .then(() => {
                  return deployUserProxyRegistry(
                    deployer,
                    IdeaTokenFactory.address,
                    MockDai.address,
                    RToken.address,
                    MockUniswapFactory.address);
                  });
                });
            });
        });
    });
  } else if (network === "kovan") {

    if(deployNewFactory) {
      console.log("Deploying new factory");
      let oldFactory = "0x0000000000000000000000000000000000000000";
      if(transferState) {
        oldFactory = oldFactoryAddress;
      }

      await deployITF(deployer, oldFactory, kovanrDaiAddress, devFund);

      if(transferState) {
        console.log("Transfering state from", oldFactoryAddress, "to", IdeaTokenFactory.address);
        const oldFactory = await IdeaTokenFactory.at(oldFactoryAddress);
        await oldFactory.transferState(IdeaTokenFactory.address);
      }
    }

    if(deployNewUserProxyRegistry) {
      console.log("Deploying new user proxy registry");
      await deployer.deploy(MockUniswapFactory);
      await deployUserProxyRegistry(
        deployer,
        IdeaTokenFactory.address,
        kovanDaiAddress,
        kovanrDaiAddress,
        MockUniswapFactory.address);
    }

    if(deployNewFactory && !deployNewUserProxyRegistry) {
      console.log("Pointing old user proxy registry to new factory at", IdeaTokenFactory.address);
      const oldUserProxyRegistry = await UserProxyRegistry.at(oldUserProxyRegistryAddress);
      await oldUserProxyRegistry.changeFactoryAddress(IdeaTokenFactory.address);
    }

  } else {

    if(deployNewFactory) {
      console.log("Deploying new factory");
      let oldFactory = "0x0000000000000000000000000000000000000000";
      if(transferState) {
        oldFactory = oldFactoryAddress;
      }

      await deployITF(deployer, oldFactory, rdaiAddress, devFund);

      if(transferState) {
        console.log("Transfering state from", oldFactoryAddress, "to", IdeaTokenFactory.address);
        const oldFactory = await IdeaTokenFactory.at(oldFactoryAddress);
        await oldFactory.transferState(IdeaTokenFactory.address);
      }
    }

    if(deployNewUserProxyRegistry) {
      console.log("Deploying new user proxy registry");
      await deployUserProxyRegistry(
        deployer,
        IdeaTokenFactory.address,
        daiAddress,
        rdaiAddress,
        uniswapFactoryAddress);
    }

    if(deployNewFactory && !deployNewUserProxyRegistry) {
      console.log("Pointing old user proxy registry to new factory at", IdeaTokenFactory.address);
      const oldUserProxyRegistry = await UserProxyRegistry.at(oldUserProxyRegistryAddress);
      await oldUserProxyRegistry.changeFactoryAddress(IdeaTokenFactory.address);
    }
  }
};
