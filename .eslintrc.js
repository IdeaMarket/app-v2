module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    extends: ["plugin:vue/essential", "@vue/prettier"],
    globals: {
        ga: true, // Google Analytics
        cordova: true,
        __statics: true,
        process: true
    },
    rules: {
        "prefer-promise-reject-errors": "off",
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
    },
    parserOptions: {
        parser: "@typescript-eslint/parser",
        sourceType: "module"
    }
};
